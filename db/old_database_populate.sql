INSERT INTO categories (category_id, category_name, category_description, photo_url)
VALUES (1, 'Italian', 'Share your love (and thoughts) for one of the most famous cusines in the world',
        'https://res.cloudinary.com/forum-photos/image/upload/v1647593349/categories-photos/italian_drujq6.jpg');
INSERT INTO categories (category_id, category_name, category_description, photo_url)
VALUES (2, 'Chinese', 'Dive into the thousand-year-old, legendary Asian cuisine',
        'https://res.cloudinary.com/forum-photos/image/upload/v1647593348/categories-photos/chinese_x5yttz.jpg');
INSERT INTO categories (category_id, category_name, category_description, photo_url)
VALUES (3, 'Indian', 'The spice capital of the world where you are only as good as your curry',
        'https://res.cloudinary.com/forum-photos/image/upload/v1647593348/categories-photos/indian_q3dzqp.jpg');
INSERT INTO categories (category_id, category_name, category_description, photo_url)
VALUES (4, 'Mexican',
        'Chilaquiles, Huevos Rancheros, Machaca, Discada, Tacos, Burritos - do you feel the pungency in the air already?',
        'https://res.cloudinary.com/forum-photos/image/upload/v1647593348/categories-photos/mexican_ig5j54.jpg');
INSERT INTO categories (category_id, category_name, category_description, photo_url)
VALUES (5, 'Bulgarian', 'Tarator, Bob chorba, Banitsa and Airyan - what more do you need in life?',
        'https://res.cloudinary.com/forum-photos/image/upload/v1647593348/categories-photos/banitsa-picture_wi8ocj.jpg');
INSERT INTO categories (category_id, category_name, category_description, photo_url)
VALUES (6, 'French',
        'From pretentious desserts to the most simplest of stews - if you know good food, you''ve had French cuisine',
        'https://res.cloudinary.com/forum-photos/image/upload/v1647593348/categories-photos/french_cqe0x1.jpg');


INSERT INTO user_roles (user_role_id, user_role_name)
VALUES (1, 'User');
INSERT INTO user_roles (user_role_id, user_role_name)
VALUES (2, 'Admin');

INSERT INTO users (user_id, first_name, last_name, email, username, password, user_role_id)
VALUES (1, 'Emilija', 'Clarke', 'emilija@mail.com', 'emilija', 'emilija', 1);
INSERT INTO users (user_id, first_name, last_name, email, username, password, user_role_id)
VALUES (2, 'Humphrey', 'Bolton', 'humphrey@mail.com', 'humphrey', 'humphrey', 1);
INSERT INTO users (user_id, first_name, last_name, email, username, password, user_role_id)
VALUES (3, 'Storm', 'Draper', 'storm@mail.com', 'storm', 'storm', 1);
INSERT INTO users (user_id, first_name, last_name, email, username, password, user_role_id)
VALUES (4, 'Logan', 'Shields', 'logan@mail.com', 'logan', 'logan', 1);
INSERT INTO users (user_id, first_name, last_name, email, username, password, user_role_id)
VALUES (5, 'Briana', 'Moran', 'briana@mail.com', 'briana', 'briana', 1);
INSERT INTO users (user_id, first_name, last_name, email, username, password, user_role_id)
VALUES (6, 'Kean', 'Deacon', 'kean@mail.com', 'kean', 'kean', 2);
INSERT INTO users (user_id, first_name, last_name, email, username, password, user_role_id)
VALUES (7, 'Ilyas', 'Peck', 'ilyas@mail.com', 'ilyas', 'ilyas', 2);
INSERT INTO users (user_id, first_name, last_name, email, username, password, user_role_id)
VALUES (777, 'Deleted', 'user', 'default@mail.com', 'default', 'default', 1);

INSERT INTO admins_info (admin_info_id, user_id, phone)
VALUES (1, 6, '0896546525');
INSERT INTO admins_info (admin_info_id, user_id, phone)
VALUES (2, 7, '0878585865');

INSERT INTO forum_project.posts (post_id, author_id, title, content, post_timestamp, category_id)
VALUES (1, 1, 'OMG BEST TIKKA MASALA EVERRR',
        'Hi guys, I just went to Restaurant India in Plovdiv and it was amazing - can''t recommend it enough. Probably the best tikka masala I''ve had in my entire life.',
        '2022-02-10 23:39:51', 3);
INSERT INTO forum_project.posts (post_id, author_id, title, content, post_timestamp, category_id)
VALUES (2, 2, 'Looking for an authentic burrito restaurant in Sofia',
        'Can you tell me of an authentican Mexican restaurant cause I want to get some good burritos - probably going to just order, not looking to actually go there',
        '2022-02-10 23:40:56', 4);
INSERT INTO forum_project.posts (post_id, author_id, title, content, post_timestamp, category_id)
VALUES (5, 3, 'Just went to Blink Pizza...',
        'I honestly am quite disappointed, the quality was not anything as advertised on the website! Blink really need to do better when it comes to delivery times as well, took 1 hour for my pizza and it was cold. 0/5 - would not recommend',
        '2022-03-18 14:36:01', 1);
INSERT INTO forum_project.posts (post_id, author_id, title, content, post_timestamp, category_id)
VALUES (6, 4, 'Opening a new restaurant in Sofia',
        'Hi everyone, my name is Li Zhang, I came to Bulgaria from China 5 years ago and I think of opening a new restaurant in my neighborhood of Lulin - all of the other places here seem so bad. Anyone got opinions whether it would sell good? Thanks!!!!',
        '2022-03-18 14:38:20', 2);
INSERT INTO forum_project.posts (post_id, author_id, title, content, post_timestamp, category_id)
VALUES (7, 5, 'First time visiting Burgas',
        'Hello citizens of the world! I am coming for my first time ever in Burgas and I want to try a small cheap local place where mostly Bulgarians visit so I can really get into the vibe. Anyone who is local to Burgas - PLEASE HELP!!!',
        '2022-03-18 14:40:42', 5);
INSERT INTO forum_project.posts (post_id, author_id, title, content, post_timestamp, category_id)
VALUES (8, 6, 'c''est quoi "La Cocotte" ???',
        'Je viens d''aller dans ce restaurant de merde et je ne pourrais pas être plus déçu des ordures qu''ils vendent comme "nourriture française" là-bas. Quiconque est français, s''il vous plaît, ne visitez jamais cet endroit...',
        '2022-03-18 14:43:33', 6);

INSERT INTO forum_project.posts (author_id, title, content, post_timestamp, category_id)
VALUES (3, 'The ultimate thread on best pizza',
        'Hi guys, please share your best recommendations for pizza in the whole country of Bulgaria - maybe add a short description on why too? :)',
        '2022-03-26 17:40:50', 1);
INSERT INTO forum_project.posts (author_id, title, content, post_timestamp, category_id)
VALUES (5, 'Pasta carbonara',
        'I really don''t understand why all of the restaurants in Sofia serve a soupy, heavy-cream-based, disgusting sauce with pasta and immediately call it a "carbonara"? Sometimes I wonder if those people have ever gone to italy or had any real italian food but they still open their italian restaurants... no probs, sure',
        '2022-03-26 17:40:50', 1);
INSERT INTO forum_project.posts (author_id, title, content, post_timestamp, category_id)
VALUES (6, 'Dominos',
        'Heyyyy everyone, I just wanted to ask the folk with the more experienced palate whether they would consider Dominos real pizza? I mean like I know its a franchise but their pizzas are still so good sometimes',
        '2022-03-26 17:40:50', 1);

INSERT INTO forum_project.comments (comment_id, post_id, author_id, content, comment_timestamp)
VALUES (1, 1, 3, 'Totally agree, best place for Indian food I''ve managed to find in Plovdiv so far',
        '2022-02-10 23:41:45');
INSERT INTO forum_project.comments (comment_id, post_id, author_id, content, comment_timestamp)
VALUES (2, 1, 2, 'IDK... honestly, I''ve had better...', '2022-02-10 23:42:06');
INSERT INTO forum_project.comments (comment_id, post_id, author_id, content, comment_timestamp)
VALUES (3, 2, 4, 'Ask Google, they should know this.', '2022-02-10 23:42:39');
INSERT INTO forum_project.comments (comment_id, post_id, author_id, content, comment_timestamp)
VALUES (4, 2, 3, 'Maybe Senior Burrito? I don''t think it''s authentic at all but it can scratch the itch for Mexican',
        '2022-02-10 23:43:20');
INSERT INTO forum_project.comments (comment_id, post_id, author_id, content, comment_timestamp)
VALUES (10, 5, 1,
        'Yeah it was much better previous years - new owners or something like that I hear? But oh well... most of my favorite pizza places have all gone downhill :(',
        '2022-03-18 14:58:06');
INSERT INTO forum_project.comments (comment_id, post_id, author_id, content, comment_timestamp)
VALUES (11, 8, 1,
        'Please refrain from posting in languages other than English. We all want to be able to understand you!',
        '2022-03-18 14:58:06');
INSERT INTO forum_project.comments (comment_id, post_id, author_id, content, comment_timestamp)
VALUES (12, 5, 5, 'please don''t say this - that''s the best pizza in Sofia!!!!', '2022-03-18 14:58:06');
INSERT INTO forum_project.comments (comment_id, post_id, author_id, content, comment_timestamp)
VALUES (13, 6, 2, 'I think that''s a great idea! Lulin needs more chinese restaurants!', '2022-03-18 14:58:06');
INSERT INTO forum_project.comments (comment_id, post_id, author_id, content, comment_timestamp)
VALUES (14, 6, 5,
        'Go for it Li, I''m sure you could make it although there are quite a few Chinese restaurants in Sofia to be honest with you but with hard work and dedication I''m sure you could beat the competition!',
        '2022-03-18 14:58:06');


INSERT INTO tags (tag_id, tag_name)
VALUES (3, 'Bulgarian');
INSERT INTO tags (tag_id, tag_name)
VALUES (8, 'Burgas');
INSERT INTO tags (tag_id, tag_name)
VALUES (1, 'Chinese');
INSERT INTO tags (tag_id, tag_name)
VALUES (5, 'French');
INSERT INTO tags (tag_id, tag_name)
VALUES (2, 'Italian');
INSERT INTO tags (tag_id, tag_name)
VALUES (9, 'Plovdiv');
INSERT INTO tags (tag_id, tag_name)
VALUES (6, 'Sofia');
INSERT INTO tags (tag_id, tag_name)
VALUES (4, 'Turkish');
INSERT INTO tags (tag_id, tag_name)
VALUES (7, 'Varna');

INSERT INTO posts_tags (relation_id, tag_id, post_id)
VALUES (1, 3, 1);
INSERT INTO posts_tags (relation_id, tag_id, post_id)
VALUES (2, 8, 1);
INSERT INTO posts_tags (relation_id, tag_id, post_id)
VALUES (3, 5, 2);
INSERT INTO posts_tags (relation_id, tag_id, post_id)
VALUES (4, 6, 1);
INSERT INTO posts_tags (relation_id, tag_id, post_id)
VALUES (5, 6, 2);

INSERT INTO posts_likes (like_id, post_id, user_id)
VALUES (1, 1, 1);
INSERT INTO posts_likes (like_id, post_id, user_id)
VALUES (2, 1, 2);
INSERT INTO posts_likes (like_id, post_id, user_id)
VALUES (3, 1, 3);
INSERT INTO posts_likes (like_id, post_id, user_id)
VALUES (4, 1, 4);
INSERT INTO posts_likes (like_id, post_id, user_id)
VALUES (5, 1, 5);
INSERT INTO posts_likes (like_id, post_id, user_id)
VALUES (6, 2, 1);
INSERT INTO posts_likes (like_id, post_id, user_id)
VALUES (7, 2, 2);
INSERT INTO posts_likes (like_id, post_id, user_id)
VALUES (8, 2, 3);
INSERT INTO posts_likes (like_id, post_id, user_id)
VALUES (9, 2, 4);
INSERT INTO posts_likes (like_id, post_id, user_id)
VALUES (10, 2, 5);


