create table categories
(
    category_id          int auto_increment
        primary key,
    category_name        varchar(20) not null,
    category_description varchar(200) null,
    photo_url            varchar(300) null,
    constraint categories_category_name_uindex
        unique (category_name)
);



create table tags
(
    tag_id   int auto_increment
        primary key,
    tag_name varchar(50) not null,
    constraint tags_tag_name_uindex
        unique (tag_name)
);

create table user_roles
(
    user_role_id   int auto_increment
        primary key,
    user_role_name varchar(15) not null,
    constraint user_types_user_type_name_uindex
        unique (user_role_name)
);

create table users
(
    user_id             int auto_increment
        primary key,
    first_name          varchar(32) not null,
    last_name           varchar(32) not null,
    email               varchar(50) not null,
    username            varchar(20) not null,
    password            varchar(20) not null,
    user_role_id        int         not null,
    profile_picture_url varchar(10000) null,
    user_is_blocked     tinyint(1) default 0 not null,
    constraint users_email_uindex
        unique (email),
    constraint users_username_uindex
        unique (username),
    constraint users_user_types_user_type_id_fk
        foreign key (user_role_id) references user_roles (user_role_id)
);

create table admins_info
(
    admin_info_id int auto_increment
        primary key,
    user_id       int null,
    phone         varchar(10) not null,
    constraint admins_info_users_user_id_fk
        foreign key (user_id) references users (user_id)
);

create table posts
(
    post_id        int auto_increment
        primary key,
    author_id      int null,
    title          varchar(64)                           not null,
    content        varchar(8192)                         not null,
    post_timestamp timestamp default current_timestamp() not null,
    category_id    int                                   not null,
    constraint posts_categories_category_id_fk
        foreign key (category_id) references categories (category_id),
    constraint posts_users_user_id_fk
        foreign key (author_id) references users (user_id)
);

create table comments
(
    comment_id        int auto_increment
        primary key,
    post_id           int                                   not null,
    author_id         int                                   not null,
    content           varchar(8192)                         not null,
    comment_timestamp timestamp default current_timestamp() not null,
    constraint comments_posts_post_id_fk
        foreign key (post_id) references posts (post_id),
    constraint comments_users_user_id_fk
        foreign key (author_id) references users (user_id)
);

create table posts_likes
(
    like_id int auto_increment
        primary key,
    post_id int not null,
    user_id int not null,
    constraint posts_likes_posts_post_id_fk
        foreign key (post_id) references posts (post_id),
    constraint posts_likes_users_user_id_fk
        foreign key (user_id) references users (user_id)
);

create table posts_tags
(
    relation_id int auto_increment
        primary key,
    tag_id      int null,
    post_id     int null,
    constraint posts_tags_posts_post_id_fk
        foreign key (post_id) references posts (post_id),
    constraint posts_tags_tags_tag_id_fk
        foreign key (tag_id) references tags (tag_id)
);

