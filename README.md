# Foodies

A Java project by Yordan Bradushev and Ivan Petkov for the learning purposes of Telerik Academy.

Foodies is the forum dedicated to discussing and sharing your most exciting (and not so much) restaurant-going experiences.

![loginPage](https://i.ibb.co/12F1pBR/login.jpg)
![categoryPage](https://i.ibb.co/h90MxZ8/categorypageview.jpg)
![editProfilePage](https://i.ibb.co/0Bm4TdM/editprofilepage.jpg)

## Prerequisites

If you want to run the project locally, you will need: IntelliJ and Java SDK

## Getting started

1. Configure application.properties file to use your personal details

2. Run the old_database_schema.sql and old_database_populate.sql files to configure the database

3. Enjoy!

## Built with

- [Spring](https://spring.io/) - Web framework used
- [Gradle](https://gradle.org/) - Dependency management
- [Bootstrap](https://getbootstrap.com/)/[Nicepage](https://nicepage.com/) - Used to generate HTML and CSS
- [Thymeleaf](https://www.thymeleaf.org/) - Used to manage the MVC

## Swagger Documentation
When you run the project, you can read api documentation on this [link](http://localhost:8080/swagger-ui/)

## Database relations

![db-schema](https://i.ibb.co/rGb3L4L/forum-db-schema.jpg)

## Authors and acknowledgment

- **Yordan Bradushev**
- **Ivan Petkov**

_One of the many dream teams in Telerik Academy who will be forever greatful to their trainers, colleagues and mentors_


