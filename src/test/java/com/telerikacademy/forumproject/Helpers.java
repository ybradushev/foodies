package com.telerikacademy.forumproject;

import com.telerikacademy.forumproject.models.*;

import java.time.LocalDateTime;
import java.util.HashSet;

public class Helpers {

    public static Users createDefaultUser() {
        Users defaultUser = createMockUser("DefaultUsername");
        defaultUser.setUserId(777);

        return defaultUser;
    }

    public static Users createMockUser(String username) {
        return createMockUser("User", username);
    }

    public static Users createMockAdmin(String username) {
        return createMockUser("Admin", username);
    }

    private static Users createMockUser(String role, String username) {
        var mockUser = new Users();

        mockUser.setUserId(1);
        mockUser.setFirstName("MockFirstName");
        mockUser.setLastName("MockLastName");
        mockUser.setEmail("mock@user.com");
        mockUser.setUsername(username);
        mockUser.setPassword("MockPassword");
        mockUser.setProfilePictureUrl("mocksite.com");
        mockUser.setUserBlocked(false);
        mockUser.setUserRole(createMockRole(role));

        return mockUser;
    }

    public static UserRole createMockRole(String role) {
        var mockRole = new UserRole();

        mockRole.setUserTypeId(1);
        mockRole.setUserTypeName(role);

        return mockRole;
    }

    public static Posts createMockPost() {
        var mockPost = new Posts();

        mockPost.setPostId(1);
        mockPost.setPostTitle("This is a mock post title");
        mockPost.setPostContent("This is a very long mock post content but " +
                "we had to extend it so that it fits the requirements.");
        mockPost.setPostTimestamp(LocalDateTime.now());
        mockPost.setPostAuthor(createMockUser("MockUsername"));
        mockPost.setCategory(createMockCategory());
        mockPost.setLikesList(new HashSet<>());

        return mockPost;
    }

    public static Categories createMockCategory() {
        var categories = new Categories();

        categories.setCategoryId(1);
        categories.setCategoryName("MockCategory");

        return categories;
    }

    public static Comments createMockComment() {
        var comment = new Comments();

        comment.setCommentId(1);
        comment.setCommentContent("Comment content");
        comment.setCommentTimestamp(LocalDateTime.now());
        comment.setCommentAuthor(createMockUser("MockUsername"));
        comment.setPost(createMockPost());

        return comment;
    }

    public static AdminsInfo createMockAdminsInfo() {
        var adminsInfo = new AdminsInfo();

        adminsInfo.setAdminsInfoId(1);
        adminsInfo.setPhone("0896456123");
        adminsInfo.setUser(createMockUser("MockUsername"));

        return adminsInfo;
    }

    public static Tags createMockTag(String tagName) {
        Tags tag = new Tags();

        tag.setTagId(1);
        tag.setTagName(tagName);

        return tag;
    }
}
