package com.telerikacademy.forumproject.services;

import com.telerikacademy.forumproject.exceptions.DuplicateEntityException;
import com.telerikacademy.forumproject.exceptions.EntityNotFoundException;
import com.telerikacademy.forumproject.exceptions.UnauthorizedOperationException;
import com.telerikacademy.forumproject.models.Users;
import com.telerikacademy.forumproject.repositories.contracts.UserRepository;
import com.telerikacademy.forumproject.services.contracts.CommentsService;
import com.telerikacademy.forumproject.services.contracts.PostsService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Optional;

import static com.telerikacademy.forumproject.Helpers.createMockAdmin;
import static com.telerikacademy.forumproject.Helpers.createMockUser;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTests {

    @Mock
    UserRepository mockUserRepository;

    @Mock
    CommentsService mockCommentsService;

    @Mock
    PostsService mockPostsService;

    @InjectMocks
    UserServiceImpl userService;


    @Test
    void getAll_should_callRepository() {
        // Arrange
        Mockito.when(mockUserRepository.getAllUsers())
                .thenReturn(new ArrayList<>());

        // Act
        userService.getAllUsers();

        // Assert
        Mockito.verify(mockUserRepository, Mockito.times(1))
                .getAllUsers();
    }

    @Test
    void getById_should_ReturnUser_when_MatchExists() {
        // Arrange
        Users mockUser = createMockUser("MockUser");
        Mockito.when(mockUserRepository.getUserById(mockUser.getUserId()))
                .thenReturn(mockUser);

        // Act
        Users result = userService.getUserById(mockUser.getUserId());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockUser.getUserId(), result.getUserId()),
                () -> Assertions.assertEquals(mockUser.getFirstName(), result.getFirstName()),
                () -> Assertions.assertEquals(mockUser.getLastName(), result.getLastName()),
                () -> Assertions.assertEquals(mockUser.getEmail(), result.getEmail()),
                () -> Assertions.assertEquals(mockUser.getUsername(), result.getUsername()),
                () -> Assertions.assertEquals(mockUser.getPassword(), result.getPassword()),
                () -> Assertions.assertEquals(mockUser.getProfilePictureUrl(), result.getProfilePictureUrl()),
                () -> Assertions.assertEquals(mockUser.isUserBlocked(), result.isUserBlocked()),
                () -> Assertions.assertEquals(mockUser.getUserRole(), result.getUserRole())
        );
    }

    @Test
    void getByUsername_should_ReturnUser_when_MatchExists() {
        // Arrange
        Users mockUser = createMockUser("MockUsername");
        Mockito.when(mockUserRepository.getUserByUsername(mockUser.getUsername()))
                .thenReturn(mockUser);

        // Act
        Users result = userService.getUserByUsername(mockUser.getUsername());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockUser.getUserId(), result.getUserId()),
                () -> Assertions.assertEquals(mockUser.getFirstName(), result.getFirstName()),
                () -> Assertions.assertEquals(mockUser.getLastName(), result.getLastName()),
                () -> Assertions.assertEquals(mockUser.getEmail(), result.getEmail()),
                () -> Assertions.assertEquals(mockUser.getUsername(), result.getUsername()),
                () -> Assertions.assertEquals(mockUser.getPassword(), result.getPassword()),
                () -> Assertions.assertEquals(mockUser.getProfilePictureUrl(), result.getProfilePictureUrl()),
                () -> Assertions.assertEquals(mockUser.isUserBlocked(), result.isUserBlocked()),
                () -> Assertions.assertEquals(mockUser.getUserRole(), result.getUserRole())
        );
    }

    @Test
    void create_should_throw_when_UserWithSameUserIdExists() {
        // Arrange
        Users mockUser = createMockUser("MockUsername");
        Mockito.when(mockUserRepository.getUserById(mockUser.getUserId()))
                .thenReturn(mockUser);

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> userService.createUser(mockUser));
    }

    @Test
    void create_should_CallRepository_when_UserWithSameUserIdDoesNotExist() {
        // Arrange
        Users mockUser = createMockUser("MockUsername");
        Mockito.when(mockUserRepository.getUserById(mockUser.getUserId()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        userService.createUser(mockUser);

        // Assert
        Mockito.verify(mockUserRepository, Mockito.times(1))
                .createUser(mockUser);
    }

    @Test
    void update_should_throw_when_UserToUpdateDoesNotEqualUserToAccess() {
        // Arrange
        Users mockUserToAccess = createMockUser("MockUsername");
        Users mockUserToUpdate = createMockUser("AnotherMockUsername");

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> userService.updateUser(mockUserToUpdate, mockUserToAccess));
    }

    @Test
    void update_should_CallRepository_when_UserToUpdateMatchesUserToAccess() {
        // Arrange
        Users mockUser = createMockUser("MockUsername");

        // Act
        userService.updateUser(mockUser, mockUser);

        // Assert
        Mockito.verify(mockUserRepository, Mockito.times(1))
                .updateUser(mockUser);
    }

    @Test
    void delete_should_throw_when_UserIdDoesNotMatchUserToAccessId() {
        // Arrange
        Users mockUser = createMockUser("MockUsername");

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> userService.delete(2, mockUser));
    }

    @Test
    void delete_should_callRepository_when_DeletingUser() {
        // Arrange
        Users mockUser = createMockUser("MockUsername");
        Mockito.doNothing().when(mockPostsService).deleteLikesByAuthor(mockUser.getUserId());
        Mockito.doNothing().when(mockCommentsService).setDefaultUserToComments(mockUser.getUserId());
        Mockito.doNothing().when(mockPostsService).makePostsAuthorDefault(mockUser.getUserId());

        // Act
        userService.delete(mockUser.getUserId(), mockUser);

        // Assert
        Mockito.verify(mockUserRepository, Mockito.times(1))
                .deleteUser(mockUser.getUserId());
    }

    @Test
    void block_should_throw_when_UserIsNotAdmin() {
        // Arrange
        Users mockUser = createMockUser("MockUsername");
        Users anotherUser = createMockUser("MockUsername");

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> userService.blockUser(mockUser, anotherUser.getUserId()));
    }

    @Test
    void block_should_throw_when_TryingToBlockAdmin() {
        // Arrange
        Users mockAdmin = createMockAdmin("MockAdmin");
        Users anotherMockAdmin = createMockAdmin("AnotherMockAdmin");
        Mockito.when(userService.getUserById(anotherMockAdmin.getUserId())).thenReturn(anotherMockAdmin);

        // Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> userService.blockUser(mockAdmin, anotherMockAdmin.getUserId()));
    }

    @Test
    void block_should_throw_when_TryingToBlockAlreadyBlockedUser() {
        // Arrange
        Users mockAdmin = createMockAdmin("MockAdmin");
        Users mockUser = createMockUser("MockUser");
        mockUser.setUserBlocked(true);
        Mockito.when(userService.getUserById(mockUser.getUserId())).thenReturn(mockUser);

        // Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> userService.blockUser(mockAdmin, mockUser.getUserId()));
    }

    @Test
    void block_should_CallRepository_when_UserCanBeBlocked() {
        // Arrange
        Users mockAdmin = createMockAdmin("MockAdmin");
        Users mockUser = createMockUser("MockUsername");
        Mockito.when(userService.getUserById(mockUser.getUserId())).thenReturn(mockUser);

        // Act
        userService.blockUser(mockAdmin, mockUser.getUserId());

        // Assert
        Mockito.verify(mockUserRepository, Mockito.times(1))
                .blockUser(mockUser.getUserId());
    }

    @Test
    void unblock_should_throw_when_OperatingUserIsNotAdmin() {
        // Arrange
        Users mockUser = createMockUser("MockUsername");

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> userService.unblockUser(mockUser, mockUser.getUserId()));
    }

    @Test
    void unblock_should_throw_when_UserIsAlreadyUnblocked() {
        // Arrange
        Users mockAdmin = createMockAdmin("MockAdmin");
        Users mockUser = createMockUser("MockUsername");
        Mockito.when(userService.getUserById(mockUser.getUserId())).thenReturn(mockUser);

        // Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> userService.unblockUser(mockAdmin, mockUser.getUserId()));
    }

    @Test
    void unblock_should_CallRepository_when_UserCanBeUnblocked() {
        // Arrange
        Users mockAdmin = createMockAdmin("MockAdmin");
        Users mockUser = createMockUser("MockUsername");
        mockUser.setUserBlocked(true);
        Mockito.when(userService.getUserById(mockUser.getUserId())).thenReturn(mockUser);
        userService.unblockUser(mockAdmin, mockUser.getUserId());

        // Act, Assert
        Mockito.verify(mockUserRepository, Mockito.times(1))
                .unblockUser(mockUser.getUserId());
    }

    @Test
    void isAdmin_should_ReturnTrue_if_UserIsAdmin() {
        // Arrange
        Users mockAdmin = createMockAdmin("MockAdmin");

        // Act, Assert
        Assertions.assertTrue(() -> userService.isAdmin(mockAdmin));
    }

    @Test
    void search_should_CallRepositorySearch_if_UserIsAdmin() {
        // Arrange
        Users mockAdmin = createMockAdmin("MockAdmin");

        // Act
        userService.search(Optional.of("search"), mockAdmin);

        // Assert
        Mockito.verify(mockUserRepository, Mockito.times(1))
                .search(Optional.of("search"));
    }

    @Test
    void search_should_CallRepositoryGetAll_if_UserIsNotAdmin() {
        // Arrange
        Users mockUser = createMockUser("MockUsername");

        // Act
        userService.search(Optional.of("search"), mockUser);

        // Assert
        Mockito.verify(mockUserRepository, Mockito.times(1))
                .getAllUsers();
    }
}
