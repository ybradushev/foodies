package com.telerikacademy.forumproject.services;

import com.telerikacademy.forumproject.models.UserRole;
import com.telerikacademy.forumproject.repositories.contracts.UserRolesRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.telerikacademy.forumproject.Helpers.createMockRole;

@ExtendWith(MockitoExtension.class)
public class UserRolesServiceImplTests {

    @Mock
    UserRolesRepository mockUserRolesRepository;

    @InjectMocks
    UserRolesServiceImpl userRolesService;

    @Test
    void getById_should_ReturnRole_when_MatchExists() {
        // Arrange
        UserRole mockRole = createMockRole("User");
        Mockito.when(mockUserRolesRepository.getRoleById(mockRole.getUserTypeId()))
                .thenReturn(mockRole);

        // Act
        UserRole result = userRolesService.getRoleById(1);

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockRole.getUserTypeId(), result.getUserTypeId()),
                () -> Assertions.assertEquals(mockRole.getUserTypeName(), result.getUserTypeName())
        );
    }

    @Test
    void addRole_should_CallRepository_when_ValidRole() {
        // Arrange
        UserRole mockRole = createMockRole("User");

        // Act
        userRolesService.addRole(mockRole);

        // Assert
        Mockito.verify(mockUserRolesRepository, Mockito.times(1))
                .addRole(mockRole);
    }
}
