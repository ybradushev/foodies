package com.telerikacademy.forumproject.services;

import com.telerikacademy.forumproject.models.Categories;
import com.telerikacademy.forumproject.repositories.contracts.CategoryRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static com.telerikacademy.forumproject.Helpers.createMockCategory;

@ExtendWith(MockitoExtension.class)
public class CategoryServiceImplTests {

    @Mock
    CategoryRepository mockRepository;

    @InjectMocks
    CategoryServiceImpl service;


    @Test
    void getAll_should_callRepository() {
        // Arrange
        Mockito.when(mockRepository.getAllCategories())
                .thenReturn(new ArrayList<>());

        // Act
        service.getAllCategories();

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1)).getAllCategories();
    }

    @Test
    void getCategoryById_should_returnCategory_when_matchExists() {
        // Arrange
        Categories mockCategory = createMockCategory();
        Mockito.when(mockRepository.getCategoryById(mockCategory.getCategoryId()))
                .thenReturn(mockCategory);

        // Act
        Categories result = service.getCategoryById(mockCategory.getCategoryId());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockCategory.getCategoryId(), result.getCategoryId()),
                () -> Assertions.assertEquals(mockCategory.getCategoryName(), result.getCategoryName())
        );
    }
}
