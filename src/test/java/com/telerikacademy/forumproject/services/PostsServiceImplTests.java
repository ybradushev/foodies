package com.telerikacademy.forumproject.services;

import com.telerikacademy.forumproject.exceptions.DuplicateEntityException;
import com.telerikacademy.forumproject.exceptions.EntityNotFoundException;
import com.telerikacademy.forumproject.exceptions.UnauthorizedOperationException;
import com.telerikacademy.forumproject.models.Comments;
import com.telerikacademy.forumproject.models.Posts;
import com.telerikacademy.forumproject.models.Users;
import com.telerikacademy.forumproject.repositories.contracts.CommentsRepository;
import com.telerikacademy.forumproject.repositories.contracts.PostsRepository;
import com.telerikacademy.forumproject.repositories.contracts.TagRepository;
import com.telerikacademy.forumproject.repositories.contracts.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.telerikacademy.forumproject.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class PostsServiceImplTests {

    @Mock
    PostsRepository postsMockRepository;

    @Mock
    CommentsRepository commentsMockRepository;

    @Mock
    TagRepository tagsMockRepository;

    @Mock
    UserRepository userRepository;

    @InjectMocks
    PostsServiceImpl postsService;


    @Test
    void getAll_should_callRepository() {
        // Arrange
        Mockito.when(postsMockRepository.getAllPosts())
                .thenReturn(new ArrayList<>());

        // Act
        postsService.getAllPosts();

        // Assert
        Mockito.verify(postsMockRepository, Mockito.times(1))
                .getAllPosts();
    }

    @Test
    void getPostById_should_returnPost_when_matchExists() {
        // Arrange
        Posts post = createMockPost();
        Mockito.when(postsMockRepository.getPostById(post.getPostId())).thenReturn(post);

        // Act
        Posts result = postsService.getPostById(post.getPostId());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(post.getPostId(), result.getPostId()),
                () -> Assertions.assertEquals(post.getPostTitle(), result.getPostTitle()),
                () -> Assertions.assertEquals(post.getPostContent(), result.getPostContent()),
                () -> Assertions.assertEquals(post.getPostAuthor(), result.getPostAuthor()),
                () -> Assertions.assertEquals(post.getCategory(), result.getCategory()),
                () -> Assertions.assertEquals(post.getPostTimestamp(), result.getPostTimestamp())
        );
    }

    @Test
    void create_should_throw_when_AuthorIsBlocked() {
        // Arrange
        Users author = createMockUser("MockUsername");
        Posts post = createMockPost();
        author.setUserBlocked(true);

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> postsService.createPost(post, author)
        );
    }

    @Test
    void create_should_throw_when_MatchExists() {
        // Arrange
        Posts post = createMockPost();
        Users author = createMockUser("MockUsername");
        Mockito.when(postsMockRepository.getPostById(post.getPostId()))
                .thenReturn(post);

        // Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> postsService.createPost(post, author));
    }

    @Test
    void create_should_callRepository_when_MatchDoesNotExist() {
        // Arrange
        Posts post = createMockPost();
        Users author = createMockUser("MockUsername");
        Mockito.when(postsMockRepository.getPostById(post.getPostId())).thenThrow(EntityNotFoundException.class);

        // Act
        postsService.createPost(post, author);

        // Assert
        Mockito.verify(postsMockRepository, Mockito.times(1)).createPost(post);
    }

    @Test
    void update_should_throw_when_AuthorIsBlocked() {
        // Arrange
        Users author = createMockUser("MockUsername");
        Posts post = createMockPost();
        author.setUserBlocked(true);

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> postsService.updatePost(post, author)
        );
    }

    @Test
    void update_should_throw_when_OperatingUserIsNotAuthor() {
        // Arrange
        Posts post = createMockPost();
        Users operatingUser = createMockUser("AnotherMockUsername");

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> postsService.updatePost(post, operatingUser));
    }

    //TODO
    @Test
    void update_should_callRepository_MatchDoesNotExist() {
        // Arrange
        Posts post = createMockPost();
        post.setPostTitle("Another post title");
        Mockito.when(postsMockRepository.getPostById(post.getPostId()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        postsService.updatePost(post, post.getPostAuthor());

        // Assert
        Mockito.verify(postsMockRepository, Mockito.times(1))
                .updatePost(post);
    }

    @Test
    void update_should_throw_when_MatchExists() {
        // Arrange
        Posts post = createMockPost();
        Mockito.when(postsMockRepository.getPostById(post.getPostId())).thenReturn(post);

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> postsService.updatePost(post, post.getPostAuthor()));
    }

    @Test
    void delete_should_throw_when_operatingUserIsNotAdmin() {
        // Arrange
        Posts post = createMockPost();
        Users operatingUser = createMockUser("AdminUsername");
        Mockito.when(postsService.getPostById(post.getPostId()))
                .thenReturn(post);

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> postsService.deletePost(post.getPostId(), operatingUser)
        );
    }

    @Test
    void delete_should_throw_when_operatingUserIsNotAuthor() {
        // Arrange
        Posts post = createMockPost();
        Users author = createMockUser("AuthorUsername");
        post.setPostAuthor(author);
        Users operatingUser = createMockUser("OperatingUsername");
        Mockito.when(postsService.getPostById(post.getPostId()))
                .thenReturn(post);

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> postsService.deletePost(post.getPostId(), operatingUser)
        );
    }

    @Test
    void delete_should_callRepository() {
        // Arrange
        Posts post = createMockPost();
        Mockito.when(postsService.getPostById(post.getPostId())).thenReturn(post);
        Mockito.doNothing().when(postsMockRepository).deleteLikesOfPost(post.getPostId());
        Mockito.doNothing().when(tagsMockRepository).deleteTagsOfPost(post.getPostId());
        Mockito.doNothing().when(commentsMockRepository).deleteCommentsOfPost(post.getPostId());

        // Act
        postsService.deletePost(post.getPostId(), post.getPostAuthor());

        // Assert
        Mockito.verify(postsMockRepository, Mockito.times(1)).deletePost(post.getPostId());
    }

    @Test
    void getPostsByAuthor_should_callRepository() {
        // Arrange
        Users author = createMockUser("AuthorUsername");
        Mockito.when(postsMockRepository.getPostByAuthor(author.getUserId()))
                .thenReturn(new ArrayList<>());

        // Act
        postsService.getPostsByAuthor(author.getUserId());

        // Assert
        Mockito.verify(postsMockRepository, Mockito.times(1))
                .getPostByAuthor(author.getUserId());
    }

    @Test
    void replyPost_should_throw_when_userIsBlocked() {
        // Arrange
        Posts post = createMockPost();
        Comments comment = createMockComment();
        Users operatingUser = createMockUser("MockUsername");
        operatingUser.setUserBlocked(true);

        //Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> postsService.replyPost(comment, operatingUser, post.getPostId())
        );
    }

    @Test
    void replyPost_should_callRepository() {
        // Arrange
        Posts post = createMockPost();
        Comments comment = createMockComment();
        Users operatingUser = createMockUser("MockUsername");

        // Act
        postsService.replyPost(comment, operatingUser, post.getPostId());

        // Assert
        Mockito.verify(postsMockRepository, Mockito.times(1))
                .replyPost(comment, operatingUser, post.getPostId());
    }

    @Test
    void deleteLikesByAuthor_should_callRepository() {
        // Arrange
        Users author = createMockUser("MockUsername");
        Mockito.when(postsService.getPostsByLikesList(author.getUserId()))
                .thenReturn(new ArrayList<>());

        // Act
        List<Posts> result = postsService.getPostsByLikesList(author.getUserId());
        postsService.deleteLikesByAuthor(author.getUserId());

        // Assert
        Mockito.verify(postsMockRepository, Mockito.times(1))
                .deleteLikesOfUser(result, author.getUserId());
    }

    @Test
    void likePost_should_throw_when_operatingUserIsBlocked() {
        // Arrange
        Posts post = createMockPost();
        Users operatingUser = createMockUser("MockUsername");
        operatingUser.setUserBlocked(true);

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> postsService.likePost(operatingUser, post.getPostId())
        );
    }

    @Test
    void likePost_should_throw_when_operatingUserAlreadyLikedPost() {
        // Arrange
        Posts post = createMockPost();
        Users operatingUser = createMockUser("MockUsername");
        post.getLikesList().add(operatingUser);
        Mockito.when(postsMockRepository.getPostById(post.getPostId()))
                .thenReturn(post);

        // Act, Assert
        Assertions.assertThrows(
                UnauthorizedOperationException.class,
                () -> postsService.likePost(operatingUser, post.getPostId())
        );
    }

    @Test
    void likePost_should_callRepository() {
        // Arrange
        Posts post = createMockPost();
        Users operatingUser = createMockUser("MockUsername");
        Mockito.when(postsMockRepository.getPostById(post.getPostId()))
                .thenReturn(post);

        // Act
        postsService.likePost(operatingUser, post.getPostId());

        // Assert
        Mockito.verify(postsMockRepository, Mockito.times(1))
                .likePost(operatingUser, post);
    }

    @Test
    void searchPostByTags_should_callRepository() {
        // Arrange
        Optional<List<String>> tags = Optional.of(new ArrayList<>());

        // Act
        postsService.searchPostByTag(tags);

        // Assert
        Mockito.verify(postsMockRepository, Mockito.times(1))
                .searchPostByTag(tags);
    }

    @Test
    void filter_should_callRepository() {
        // Arrange
        Optional<String> title = Optional.of(Mockito.anyString());
        Optional<String> startDate = Optional.of(Mockito.anyString());
        Optional<String> endDate = Optional.of(Mockito.anyString());
        Optional<String> sort = Optional.of(Mockito.anyString());
        Optional<Integer> authorId = Optional.of(Mockito.anyInt());
        Optional<Integer> categoryId = Optional.of(Mockito.anyInt());

        // Act
        postsService.filter(title, authorId, categoryId, startDate, endDate, sort);

        // Assert
        Mockito.verify(postsMockRepository, Mockito.times(1))
                .filter(title, authorId, categoryId, startDate, endDate, sort);
    }

    @Test
    void getRecentlyCreatedPosts_should_callRepository() {
        // Arrange
        Mockito.when(postsMockRepository.getRecentlyCreatedPosts())
                .thenReturn(new ArrayList<>());

        // Act
        postsService.getRecentlyCreatedPosts();

        // Assert
        Mockito.verify(postsMockRepository, Mockito.times(1))
                .getRecentlyCreatedPosts();
    }

    @Test
    void makePostsAuthorDefault_should_callRepository() {
        // Arrange
        Users author = createMockUser("MockUsername");
        Users defaultUser = createDefaultUser();
        Posts post1 = createMockPost();
        post1.setPostAuthor(author);
        List<Posts> result = new ArrayList<>();
        result.add(post1);
        Mockito.when(postsService.getPostsByAuthor(author.getUserId()))
                .thenReturn(result);
        Mockito.when(userRepository.getUserById(defaultUser.getUserId()))
                .thenReturn(defaultUser);

        // Act
        postsService.makePostsAuthorDefault(author.getUserId());

        // Assert
        Mockito.verify(postsMockRepository, Mockito.times(1))
                .makePostWithDefaultUser(result);
    }
}
