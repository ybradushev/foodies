package com.telerikacademy.forumproject.services;

import com.telerikacademy.forumproject.exceptions.DuplicateEntityException;
import com.telerikacademy.forumproject.exceptions.UnauthorizedOperationException;
import com.telerikacademy.forumproject.models.AdminsInfo;
import com.telerikacademy.forumproject.models.Users;
import com.telerikacademy.forumproject.repositories.contracts.AdminsInfoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static com.telerikacademy.forumproject.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class AdminsInfoServiceImplTests {

    @Mock
    AdminsInfoRepository adminsInfoRepository;

    @Mock
    UserServiceImpl userService;

    @InjectMocks
    AdminsInfoServiceImpl adminsInfoService;


    @Test
    void getAllAdminsInfo_should_callRepository() {
        // Arrange
        Mockito.when(adminsInfoRepository.getAllAdminsInfo())
                .thenReturn(new ArrayList<>());

        // Act
        adminsInfoService.getAllAdminsInfo();

        // Assert
        Mockito.verify(adminsInfoRepository, Mockito.times(1))
                .getAllAdminsInfo();
    }

    @Test
    void createAdminsInfo_should_callRepository() {
        // Arrange
        AdminsInfo adminsInfo = createMockAdminsInfo();
        Mockito.when(adminsInfoRepository.createAdmin(adminsInfo))
                .thenReturn(adminsInfo);

        // Act
        adminsInfoService.createAdminsInfo(adminsInfo);

        // Arrange
        Mockito.verify(adminsInfoRepository, Mockito.times(1))
                .createAdmin(adminsInfo);
    }

    @Test
    void promoteUser_should_throw_when_operatingUserIsNotAdmin() {
        // Arrange
        Users operatingUser = createMockUser("MockUsername");
        Users usersToPromote = createMockUser("AnotherMockUsername");
        AdminsInfo adminsInfo = createMockAdminsInfo();

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> adminsInfoService.promoteUser(operatingUser, usersToPromote.getUserId(), adminsInfo)
        );
    }

    @Test
    void promoteUser_should_throw_when_userToPromoteIsAlreadyAdmin() {
        // Arrange
        Users operatingUser = createMockAdmin("MockUsername");
        Users usersToPromote = createMockAdmin("AnotherMockUsername");
        AdminsInfo adminsInfo = createMockAdminsInfo();
        Mockito.when(userService.isAdmin(operatingUser))
                .thenReturn(true);
        Mockito.when(userService.isAdmin(usersToPromote))
                .thenReturn(true);
        Mockito.when(userService.getUserById(usersToPromote.getUserId()))
                .thenReturn(usersToPromote);

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> adminsInfoService.promoteUser(operatingUser, usersToPromote.getUserId(), adminsInfo)
        );
    }

    @Test
    void promoteUser_should_callRepository() {
        // Arrange
        Users operatingUser = createMockAdmin("MockUsername");
        Users usersToPromote = createMockUser("AnotherMockUsername");
        AdminsInfo adminsInfo = createMockAdminsInfo();
        Mockito.when(userService.isAdmin(operatingUser))
                .thenReturn(true);
        Mockito.when(userService.getUserById(usersToPromote.getUserId()))
                .thenReturn(usersToPromote);
        Mockito.when(adminsInfoRepository.createAdmin(adminsInfo))
                .thenReturn(adminsInfo);

        // Act
        adminsInfoService.promoteUser(operatingUser, usersToPromote.getUserId(), adminsInfo);

        // Assert
        Mockito.verify(adminsInfoRepository, Mockito.times(1))
                .promoteUser(adminsInfo, usersToPromote);
    }
}
