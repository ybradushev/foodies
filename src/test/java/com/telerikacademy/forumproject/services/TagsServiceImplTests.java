package com.telerikacademy.forumproject.services;

import com.telerikacademy.forumproject.exceptions.DuplicateEntityException;
import com.telerikacademy.forumproject.exceptions.EntityNotFoundException;
import com.telerikacademy.forumproject.exceptions.UnauthorizedOperationException;
import com.telerikacademy.forumproject.models.Tags;
import com.telerikacademy.forumproject.models.Users;
import com.telerikacademy.forumproject.repositories.contracts.TagRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static com.telerikacademy.forumproject.Helpers.createMockTag;
import static com.telerikacademy.forumproject.Helpers.createMockUser;

@ExtendWith(MockitoExtension.class)
public class TagsServiceImplTests {

    @Mock
    TagRepository tagMockRepository;

    @InjectMocks
    TagServiceImpl tagService;

    @Test
    void getAll_should_callRepository() {
        // Arrange
        Mockito.when(tagMockRepository.getAllTags())
                .thenReturn(new ArrayList<>());

        // Act
        tagService.getAllTags();

        // Assert
        Mockito.verify(tagMockRepository, Mockito.times(1))
                .getAllTags();
    }

    @Test
    void getTagById_should_returnTag_when_tagExists() {
        // Arrange
        Tags tag = createMockTag("MockTagName");
        Mockito.when(tagMockRepository.getTagById(tag.getTagId()))
                .thenReturn(tag);

        // Act
        Tags result = tagService.getTagById(tag.getTagId());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(result.getTagId(), tag.getTagId()),
                () -> Assertions.assertEquals(result.getTagName(), tag.getTagName())
        );
    }

    @Test
    void create_should_throw_when_operatingUserIsBlocked() {
        // Arrange
        Users operatingUser = createMockUser("MockUsername");
        operatingUser.setUserBlocked(true);
        Tags tag = createMockTag("MockTagName");

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> tagService.create(tag, operatingUser)
        );
    }

    @Test
    void create_should_throw_when_matchExists() {
        // Arrange
        Users operatingUser = createMockUser("MockUsername");
        Tags tag = createMockTag("MockTagName");
        Mockito.when(tagMockRepository.getTagByName(tag.getTagName()))
                .thenReturn(tag);

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> tagService.create(tag, operatingUser)
        );
    }

    @Test
    void create_should_callRepository() {
        // Arrange
        Users operatingUser = createMockUser("MockUsername");
        Tags tag = createMockTag("MockTagName");
        Mockito.when(tagMockRepository.getTagByName(tag.getTagName()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        tagService.create(tag, operatingUser);

        // Assert
        Mockito.verify(tagMockRepository, Mockito.times(1))
                .create(tag);
    }

    @Test
    void update_should_throw_when_operatingUserIsBlocked() {
        // Arrange
        Users operatingUser = createMockUser("MockUsername");
        operatingUser.setUserBlocked(true);
        Tags tag = createMockTag("MockTagName");

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> tagService.update(tag, operatingUser)
        );
    }

    @Test
    void update_should_throw_when_matchExists() {
        // Arrange
        Users operatingUser = createMockUser("MockUsername");
        Tags tag = createMockTag("MockTagName");
        Mockito.when(tagMockRepository.getTagByName(tag.getTagName()))
                .thenReturn(tag);

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> tagService.update(tag, operatingUser)
        );
    }

    @Test
    void update_should_callRepository() {
        // Arrange
        Users operatingUser = createMockUser("MockUsername");
        Tags tag = createMockTag("MockTagName");
        Mockito.when(tagMockRepository.getTagByName(tag.getTagName()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        tagService.update(tag, operatingUser);

        // Assert
        Mockito.verify(tagMockRepository, Mockito.times(1))
                .update(tag);
    }

    @Test
    void delete_should_throw_when_operatingUserIsBlocked() {
        // Arrange
        Users operatingUser = createMockUser("MockUsername");
        operatingUser.setUserBlocked(true);
        Tags tag = createMockTag("MockTagName");

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> tagService.delete(tag.getTagId(), operatingUser)
        );
    }

    @Test
    void delete_should_callRepository() {
        //Arrange
        Users operatingUser = createMockUser("MockUsername");
        Tags tag = createMockTag("MockTagName");
        Mockito.when(tagMockRepository.getTagById(tag.getTagId()))
                .thenReturn(tag);
        Mockito.doNothing().when(tagMockRepository).deleteTagsList(tag);

        //Act
        tagService.delete(tag.getTagId(), operatingUser);

        //Assert
        Mockito.verify(tagMockRepository, Mockito.times(1))
                .deleteTag(tag);
    }

}
