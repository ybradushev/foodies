package com.telerikacademy.forumproject.services;

import com.telerikacademy.forumproject.exceptions.DuplicateEntityException;
import com.telerikacademy.forumproject.exceptions.EntityNotFoundException;
import com.telerikacademy.forumproject.exceptions.UnauthorizedOperationException;
import com.telerikacademy.forumproject.models.Comments;
import com.telerikacademy.forumproject.models.Posts;
import com.telerikacademy.forumproject.models.Users;
import com.telerikacademy.forumproject.repositories.contracts.CommentsRepository;
import com.telerikacademy.forumproject.repositories.contracts.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.forumproject.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class CommentsServiceImplTests {

    @Mock
    CommentsRepository mockCommentsRepository;

    @Mock
    UserRepository mockUserRepository;

    @InjectMocks
    CommentsServiceImpl commentsService;


    @Test
    void getById_should_ReturnComment_when_MatchExists() {
        // Arrange
        Comments mockComment = createMockComment();
        Mockito.when(mockCommentsRepository.getCommentById(mockComment.getCommentId()))
                .thenReturn(mockComment);

        // Act
        Comments result = commentsService.getCommentById(mockComment.getCommentId());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(result.getCommentId(), mockComment.getCommentId()),
                () -> Assertions.assertEquals(result.getCommentAuthor(), mockComment.getCommentAuthor()),
                () -> Assertions.assertEquals(result.getCommentContent(), mockComment.getCommentContent()),
                () -> Assertions.assertEquals(result.getCommentTimestamp(), mockComment.getCommentTimestamp()),
                () -> Assertions.assertEquals(result.getPost(), mockComment.getPost())
        );
    }

    @Test
    void getCommentsOfPost_should_CallRepository_when_ValidPostId() {
        // Arrange
        Posts mockPost = createMockPost();
        Mockito.when(mockCommentsRepository.getCommentOfPost(mockPost.getPostId()))
                .thenReturn(new ArrayList<>());

        // Act
        commentsService.getCommentsOfPost(mockPost.getPostId());

        // Assert
        Mockito.verify(mockCommentsRepository, Mockito.times(1))
                .getCommentOfPost(mockPost.getPostId());
    }

    @Test
    void addCommentToPost_should_CallRepository_when_ValidComment() {
        // Arrange
        Comments mockComment = createMockComment();

        // Act
        commentsService.addCommentToPost(mockComment);

        // Assert
        Mockito.verify(mockCommentsRepository, Mockito.times(1))
                .addCommentToPost(mockComment);
    }

    @Test
    void setDefaultUser_should_CallRepository_when_ValidUserId() {
        // Arrange
        Users mockUser = createMockUser("MockUsername");
        Users mockDefaultUser = createMockUser("DefaultName");
        List<Comments> mockCommentsList = new ArrayList<>();
        Mockito.when(commentsService.getCommentsByAuthor(mockUser.getUserId()))
                .thenReturn(mockCommentsList);
        Mockito.when(mockUserRepository.getUserById(777))
                .thenReturn(mockDefaultUser);

        // Act
        commentsService.setDefaultUserToComments(mockUser.getUserId());

        // Assert
        Mockito.verify(mockCommentsRepository, Mockito.times(1))
                .setDefaultUserToComments(mockCommentsList);
    }

    @Test
    void getCommentsByAuthor_should_CallRepository_when_UserIdIsValid() {
        // Arrange
        Users mockUser = createMockUser("MockUsername");

        // Act
        commentsService.getCommentsByAuthor(mockUser.getUserId());

        // Assert
        Mockito.verify(mockCommentsRepository, Mockito.times(1))
                .getCommentsByAuthor(mockUser.getUserId());
    }

    @Test
    void updatePost_should_throw_when_OperatingUserIsBlocked() {
        // Arrange
        Users mockUser = createMockUser("MockUsername");
        mockUser.setUserBlocked(true);
        Comments mockComment = createMockComment();
        Posts mockPost = createMockPost();

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> commentsService.updatePost(mockComment, mockUser, mockPost));
    }

    @Test
    void updatePost_should_throw_when_OperatingUserIsNotCommentAuthor() {
        // Arrange
        Users mockUser = createMockUser("MockUsername");
        Users anotherMockUser = createMockUser("AnotherMockUsername");
        Comments mockComment = createMockComment();
        mockComment.setCommentAuthor(anotherMockUser);
        Posts mockPost = createMockPost();

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> commentsService.updatePost(mockComment, mockUser, mockPost));
    }

    @Test
    void updatePost_should_throw_when_CommentContentAlreadyExists() {
        // Arrange
        Users mockUser = createMockUser("MockUsername");
        Comments mockComment = createMockComment();
        Posts mockPost = createMockPost();
        Mockito.when(mockCommentsRepository.getCommentById(mockComment.getCommentId()))
                .thenReturn(mockComment);

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> commentsService.updatePost(mockComment, mockUser, mockPost));
    }

    @Test
    void updatePost_should_CallRepository_when_AllInfoIsValid() {
        // Arrange
        Users mockUser = createMockUser("MockUsername");
        Comments mockComment = createMockComment();
        Posts mockPost = createMockPost();
        Mockito.when(mockCommentsRepository.getCommentById(mockComment.getCommentId()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        commentsService.updatePost(mockComment, mockUser, mockPost);

        // Assert
        Mockito.verify(mockCommentsRepository, Mockito.times(1))
                .updateComment(mockComment);
    }
}
