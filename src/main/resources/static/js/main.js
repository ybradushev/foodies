$('document').ready(function(){
    $('#edit-button').on('click',function(event){

        event.preventDefault();

        var href = $(this).attr('href');

        $.get(href, function (comment ,status){
            $('#editContent').val(comment.commentContent)
        });

        $('#editModal').modal();
    });
});