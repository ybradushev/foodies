package com.telerikacademy.forumproject.utils.mappers;

import com.telerikacademy.forumproject.models.Tags;
import com.telerikacademy.forumproject.models.dto.TagDTO;
import com.telerikacademy.forumproject.services.contracts.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TagMapper {

    private final TagService tagService;

    @Autowired
    public TagMapper(TagService tagService) {
        this.tagService = tagService;
    }

    public Tags createDTOToObject(TagDTO dto) {
        Tags tag = new Tags();

        tag.setTagName(dto.getTagName());

        return tag;
    }

    public Tags updateDTOToObject(TagDTO dto, int tagId) {
        Tags tag = tagService.getTagById(tagId);

        tag.setTagName(dto.getTagName());

        return tag;
    }
}
