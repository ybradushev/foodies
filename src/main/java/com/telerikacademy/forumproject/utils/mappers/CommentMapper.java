package com.telerikacademy.forumproject.utils.mappers;

import com.telerikacademy.forumproject.models.Comments;
import com.telerikacademy.forumproject.models.dto.CreateCommentDTO;
import com.telerikacademy.forumproject.models.dto.UpdateCommentDTO;
import com.telerikacademy.forumproject.services.contracts.CommentsService;
import com.telerikacademy.forumproject.services.contracts.PostsService;
import com.telerikacademy.forumproject.services.contracts.UserService;
import org.hibernate.sql.Update;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class CommentMapper {
    private final PostsService postsService;
    private final CommentsService commentsService;
    private final UserService userService;

    @Autowired
    public CommentMapper(PostsService postsService, CommentsService commentsService, UserService userService) {
        this.postsService = postsService;
        this.commentsService = commentsService;
        this.userService = userService;
    }

    public Comments dtoToObject(CreateCommentDTO commentDTO) {
        Comments comment = new Comments();

        comment.setCommentContent(commentDTO.getContent());
        comment.setPost(postsService.getPostById(commentDTO.getPostId()));
        comment.setCommentAuthor(userService.getUserById(commentDTO.getAuthorId()));
        comment.setCommentTimestamp(LocalDateTime.now());

        return comment;
    }

    public Comments updateDTOToObject(UpdateCommentDTO commentDTO, int commentId) {
        Comments comments = commentsService.getCommentById(commentId);

        comments.setCommentContent(commentDTO.getContent());

        return comments;
    }

    public UpdateCommentDTO toDto(Comments comment){
        UpdateCommentDTO commentDTO = new UpdateCommentDTO();

        commentDTO.setContent(comment.getCommentContent());

        return commentDTO;
    }
}
