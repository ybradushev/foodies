package com.telerikacademy.forumproject.utils.mappers;

import com.telerikacademy.forumproject.models.AdminsInfo;
import com.telerikacademy.forumproject.models.UserRole;
import com.telerikacademy.forumproject.models.Users;
import com.telerikacademy.forumproject.models.dto.RegisterDTO;
import com.telerikacademy.forumproject.models.dto.UpdateUserDTO;
import com.telerikacademy.forumproject.services.contracts.UserRolesService;
import com.telerikacademy.forumproject.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {
    private final String DEFAULT_USER_PHOTO = "https://res.cloudinary.com/forum-photos/image/upload/v1646903531/profile-photos/lshk9co9rv40m6ynbeeh.webp";

    private final UserRolesService userRolesService;
    private final UserService userService;

    @Autowired
    public UserMapper(UserRolesService userRolesService, UserService userService) {
        this.userRolesService = userRolesService;
        this.userService = userService;
    }


    public Users createDTOToObject(RegisterDTO userDTO) {
        Users user = new Users();

        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        user.setEmail(userDTO.getEmail());
        user.setUsername(userDTO.getUsername());
        user.setPassword(userDTO.getPassword());

        user.setUserBlocked(false);
        user.setProfilePictureUrl(DEFAULT_USER_PHOTO);

        UserRole userRole = userRolesService.getRoleById(1);
        user.setUserRole(userRole);

        return user;
    }

    public Users updateDTOToObject(UpdateUserDTO userDTO, int userId) {
        Users user = userService.getUserById(userId);

        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        user.setEmail(userDTO.getEmail());
        user.setPassword(userDTO.getPassword());

        return user;
    }

    public UpdateUserDTO toDto(Users user) {
        UpdateUserDTO userDTO = new UpdateUserDTO();

        userDTO.setFirstName(user.getFirstName());
        userDTO.setLastName(user.getLastName());
        userDTO.setEmail(user.getEmail());
        userDTO.setPassword(user.getPassword());
        userDTO.setPhotoUrl(user.getProfilePictureUrl());

        return userDTO;
    }

    public Users fromDto(UpdateUserDTO dto, int userId) {
        return updateDTOToObject(dto, userId);
    }
}
