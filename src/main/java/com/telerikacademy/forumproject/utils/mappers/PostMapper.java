package com.telerikacademy.forumproject.utils.mappers;

import com.telerikacademy.forumproject.models.Categories;
import com.telerikacademy.forumproject.models.Posts;
import com.telerikacademy.forumproject.models.Tags;
import com.telerikacademy.forumproject.models.Users;
import com.telerikacademy.forumproject.models.dto.CreatePostDTO;
import com.telerikacademy.forumproject.models.dto.UpdatePostDTO;
import com.telerikacademy.forumproject.services.contracts.CategoryService;
import com.telerikacademy.forumproject.services.contracts.PostsService;
import com.telerikacademy.forumproject.services.contracts.TagService;
import com.telerikacademy.forumproject.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class PostMapper {

    private final PostsService postsService;
    private final UserService userService;
    private final CategoryService categoryService;
    private final TagService tagService;


    @Autowired
    public PostMapper(PostsService postsService, UserService userService, CategoryService categoryService, TagService tagService) {
        this.postsService = postsService;
        this.userService = userService;
        this.categoryService = categoryService;
        this.tagService = tagService;
    }

    public Posts createDTOToObject(CreatePostDTO postDTO) {
        Posts post = new Posts();

        Set<Tags> tags = new HashSet<>();
        for (Integer id : postDTO.getTagsIds()) {
            tags.add(tagService.getTagById(id));
        }

        Categories category = categoryService.getCategoryById(postDTO.getCategoryId());
        Users user = userService.getUserById(postDTO.getPostAuthorId());

        post.setPostTitle(postDTO.getPostTitle());
        post.setPostContent(postDTO.getPostContent());
        post.setPostAuthor(user);
        post.setTagsList(tags);
        post.setCategory(category);

        return post;
    }

    public Posts updateDTOToObject(UpdatePostDTO postDTO, int postId) {
        Posts post = postsService.getPostById(postId);

        Set<Tags> tags = new HashSet<>();
        for (Integer id : postDTO.getTagsIds()) {
            tags.add(tagService.getTagById(id));
        }

        Categories category = categoryService.getCategoryById(postDTO.getCategoryId());

        post.setPostTitle(postDTO.getPostTitle());
        post.setPostContent(postDTO.getPostContent());
        post.setTagsList(tags);
        post.setCategory(category);

        return post;
    }
}
