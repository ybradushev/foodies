package com.telerikacademy.forumproject.utils.mappers;

import com.telerikacademy.forumproject.models.AdminsInfo;
import com.telerikacademy.forumproject.models.dto.CreateAdminsInfoDTO;
import com.telerikacademy.forumproject.services.contracts.AdminsInfoService;
import org.springframework.stereotype.Component;

@Component
public class AdminsInfoMapper {

    private final AdminsInfoService adminsInfoService;

    public AdminsInfoMapper(AdminsInfoService adminsInfoService) {
        this.adminsInfoService = adminsInfoService;
    }

    public AdminsInfo createDTOToObject(CreateAdminsInfoDTO adminsInfoDTO) {
        AdminsInfo adminsInfo = new AdminsInfo();

        adminsInfo.setPhone(adminsInfoDTO.getPhoneNumber());

        return adminsInfo;
    }


}
