package com.telerikacademy.forumproject.models;

import javax.persistence.*;

@Entity
@Table(name = "user_roles")
public class UserRole {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_role_id")
    private int userRoleId;

    @Column(name = "user_role_name")
    private String userRoleName;

    public UserRole() {
    }

    public UserRole(int userTypeId, String userTypeName) {
        this.userRoleId = userTypeId;
        this.userRoleName = userTypeName;
    }

    public int getUserTypeId() {
        return userRoleId;
    }

    public void setUserTypeId(int userTypeId) {
        this.userRoleId = userTypeId;
    }

    public String getUserTypeName() {
        return userRoleName;
    }

    public void setUserTypeName(String userTypeName) {
        this.userRoleName = userTypeName;
    }
}
