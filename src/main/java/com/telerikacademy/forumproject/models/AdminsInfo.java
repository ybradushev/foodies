package com.telerikacademy.forumproject.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "admins_info")
public class AdminsInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "admin_info_id")
    private int adminsInfoId;

    @Column(name = "phone")
    private String phone;

    @JsonIgnore
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    private Users user;

    public AdminsInfo() {
    }

    public AdminsInfo(int adminsInfoId, String phone, Users user) {
        this.adminsInfoId = adminsInfoId;
        this.phone = phone;
        this.user = user;
    }

    public AdminsInfo(String phone, Users user) {
        this.phone = phone;
        this.user = user;
    }

    public int getAdminsInfoId() {
        return adminsInfoId;
    }

    public void setAdminsInfoId(int adminsInfoId) {
        this.adminsInfoId = adminsInfoId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }
}
