package com.telerikacademy.forumproject.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "tags")
public class Tags {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "tag_id")
    private int tagId;

    @Column(name = "tag_name")
    private String tagName;

    @JsonIgnore
    @ManyToMany(mappedBy = "tagsList", fetch = FetchType.EAGER)
    private Set<Posts> posts;


    public Tags() {
    }

    public Tags(int tagId, String tagName) {
        this.tagId = tagId;
        this.tagName = tagName;
    }

    public Tags(int tagId, String tagName, Set<Posts> posts) {
        this.tagId = tagId;
        this.tagName = tagName;
        this.posts = posts;
    }

    public int getTagId() {
        return tagId;
    }

    public void setTagId(int tagId) {
        this.tagId = tagId;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }


    public Set<Posts> getPosts() {
        return posts;
    }

    public void setPosts(Set<Posts> posts) {
        this.posts = posts;
    }
}
