package com.telerikacademy.forumproject.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "posts")
public class Posts {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "post_id")
    private int postId;

    @Column(name = "title")
    private String postTitle;

    @Column(name = "content")
    private String postContent;

    @Column(name = "post_timestamp")
    private LocalDateTime postTimestamp;

    @JsonIgnore
    @OneToMany(mappedBy = "post", fetch = FetchType.EAGER)
    @OrderBy("commentTimestamp ASC")
    private Set<Comments> comments;

    @ManyToOne
    @JoinColumn(name = "author_id")
    private Users postAuthor;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private Categories category;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "posts_tags",
            joinColumns = @JoinColumn(name = "post_id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id")
    )
    private Set<Tags> tagsList;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "posts_likes",
            joinColumns = @JoinColumn(name = "post_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id")
    )
    private Set<Users> likesList;

    public Posts() {
    }

    public Posts(int postId, String postTitle, String postContent, Users postAuthor, Categories category, LocalDateTime postTimestamp) {
        this.postId = postId;
        this.postTitle = postTitle;
        this.postContent = postContent;
        this.postTimestamp = postTimestamp;
        this.postAuthor = postAuthor;
        this.category = category;
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public String getPostContent() {
        return postContent;
    }

    public void setPostContent(String postContent) {
        this.postContent = postContent;
    }

    public LocalDateTime getPostTimestamp() {
        return postTimestamp;
    }

    public String getPostTimestampFormatted() {
        DateTimeFormatter zonedFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm");
        return getPostTimestamp().format(zonedFormatter);
    }

    public void setPostTimestamp(LocalDateTime postTimestamp) {
        this.postTimestamp = postTimestamp;
    }

    public Set<Comments> getComments() {
        return comments;
    }

    public void setComments(Set<Comments> comments) {
        this.comments = comments;
    }

    public void addComments(Comments comment) {
        this.comments.add(comment);
    }

    public Users getPostAuthor() {
        return postAuthor;
    }

    public void setPostAuthor(Users postAuthor) {
        this.postAuthor = postAuthor;
    }

    public Categories getCategory() {
        return category;
    }

    public void setCategory(Categories category) {
        this.category = category;
    }

    public Set<Tags> getTagsList() {
        return tagsList;
    }

    public void setTagsList(Set<Tags> tagsList) {
        this.tagsList = tagsList;
    }

    public Set<Users> getLikesList() {
        return likesList;
    }

    public void setLikesList(Set<Users> likesList) {
        this.likesList = likesList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Posts posts = (Posts) o;
        return postId == posts.postId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(postId);
    }
}
