package com.telerikacademy.forumproject.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.Objects;

@Entity
@Table(name = "comments")
public class Comments {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "comment_id")
    private int commentId;

    @Column(name = "content")
    private String commentContent;

    @Column(name = "comment_timestamp")
    private LocalDateTime commentTimestamp;

    @OneToOne
    @JoinColumn(name = "author_id")
    private Users commentAuthor;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "post_id", nullable = false)
    private Posts post;

    public Comments() {
    }

    public Comments(int commentId, String commentContent, Users commentAuthor, LocalDateTime commentTimestamp) {
        this.commentId = commentId;
        this.commentContent = commentContent;
        this.commentAuthor = commentAuthor;
        this.commentTimestamp = commentTimestamp;
    }

    public int getCommentId() {
        return commentId;
    }

    public void setCommentId(int commentId) {
        this.commentId = commentId;
    }

    public String getCommentContent() {
        return commentContent;
    }

    public void setCommentContent(String commentContent) {
        this.commentContent = commentContent;
    }

    public LocalDateTime getCommentTimestamp() {
        return commentTimestamp;
    }

    public String getCommentTimestampFormatted() {
        DateTimeFormatter zonedFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm");
        return getCommentTimestamp().format(zonedFormatter);
    }

    public void setCommentTimestamp(LocalDateTime commentTimestamp) {
        this.commentTimestamp = commentTimestamp;
    }

    public Users getCommentAuthor() {
        return commentAuthor;
    }

    public void setCommentAuthor(Users commentAuthor) {
        this.commentAuthor = commentAuthor;
    }

    public Posts getPost() {
        return post;
    }

    public void setPost(Posts post) {
        this.post = post;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Comments comments = (Comments) o;
        return commentId == comments.commentId && Objects.equals(commentContent, comments.commentContent) && Objects.equals(commentTimestamp, comments.commentTimestamp) && Objects.equals(commentAuthor, comments.commentAuthor) && Objects.equals(post, comments.post);
    }

    @Override
    public int hashCode() {
        return Objects.hash(commentId, commentContent, commentTimestamp, commentAuthor, post);
    }
}
