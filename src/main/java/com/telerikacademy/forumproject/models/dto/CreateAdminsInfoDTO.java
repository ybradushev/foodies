package com.telerikacademy.forumproject.models.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CreateAdminsInfoDTO {

    @NotNull(message = "Phone number cannot be empty")
    @Size(min = 10, max = 10, message = "Phone number should be exactly 10 characters")
    private String phoneNumber;

    public CreateAdminsInfoDTO() {
    }

    public CreateAdminsInfoDTO(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
