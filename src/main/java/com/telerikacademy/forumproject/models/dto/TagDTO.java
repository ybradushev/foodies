package com.telerikacademy.forumproject.models.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class TagDTO {

    @NotNull(message = "Tag cannot be empty")
    @Size(min = 2, max = 50, message = "Tag name should be between 2 and 50 characters")
    @Pattern(regexp = "^[a-z]+$", message = "Tags can be lowercase only")
    private String tagName;

    public TagDTO() {
    }

    public TagDTO(String tagName) {
        this.tagName = tagName;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }
}
