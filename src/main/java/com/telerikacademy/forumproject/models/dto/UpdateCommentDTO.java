package com.telerikacademy.forumproject.models.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UpdateCommentDTO {

    @NotNull(message = "Comment content cannot be null")
    @Size(min = 4, max = 8192, message = "Comment content should be between 4 and 8192 symbols")
    private String content;

    public UpdateCommentDTO() {
    }

    public UpdateCommentDTO(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
