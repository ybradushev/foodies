package com.telerikacademy.forumproject.models.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

public class CreatePostDTO {

    @NotNull(message = "Post title cannot be empty")
    @Size(min = 16, max = 64, message = "Post title should be between 16 and 64 characters")
    private String postTitle;

    @NotNull(message = "Post content cannot be empty")
    @Size(min = 32, max = 8192, message = "Post content should be between 32 and 8192 characters")
    private String postContent;

    @NotNull(message = "Post author cannot be empty")
    private int postAuthorId;

    @NotNull(message = "Post category cannot be empty")
    private int categoryId;

    private List<Integer> tagsIds;

    public CreatePostDTO() {
    }

    public CreatePostDTO(String postTitle, String postContent, int postAuthorId, int categoryId, List<Integer> tagsIds) {
        this.postTitle = postTitle;
        this.postContent = postContent;
        this.postAuthorId = postAuthorId;
        this.categoryId = categoryId;
        this.tagsIds = tagsIds;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public String getPostContent() {
        return postContent;
    }

    public void setPostContent(String postContent) {
        this.postContent = postContent;
    }

    public int getPostAuthorId() {
        return postAuthorId;
    }

    public void setPostAuthorId(int postAuthorId) {
        this.postAuthorId = postAuthorId;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public List<Integer> getTagsIds() {
        return tagsIds;
    }

    public void setTagsIds(List<Integer> tagsIds) {
        this.tagsIds = tagsIds;
    }
}
