package com.telerikacademy.forumproject.models.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CreateCommentDTO {

    @NotNull(message = "Comment content cannot be null")
    @Size(min = 4, max = 8192, message = "Comment content should be between 4 and 8192 symbols")
    private String content;

    @NotNull(message = "Post id cannot be null")
    private int postId;

    @NotNull(message = "Author id cannot be null")
    private int authorId;

    public CreateCommentDTO() {
    }

    public CreateCommentDTO(String content, int postId, int authorId) {
        this.content = content;
        this.postId = postId;
        this.authorId = authorId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public int getAuthorId() {
        return authorId;
    }

    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }
}
