package com.telerikacademy.forumproject.models.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class CreateUserDTO {

    @NotNull(message = "First name cannot be empty")
    @Size(min = 4, max = 32, message = "First name should be between 4 and 32 characters")
    private String firstName;

    @NotNull(message = "Last name cannot be empty")
    @Size(min = 4, max = 32, message = "Last name should be between 4 and 32 characters")
    private String lastName;

    @NotNull(message = "Email cannot be empty")
    @Email(message = "Email is not valid", regexp = "^(.+)@(.+)$")
    private String email;

    @NotNull(message = "Username cannot be empty")
    private String username;

    @NotNull(message = "Password cannot be empty")
    @Pattern(message = "Password is not valid", regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&+=])(?=\\S+$).{6,}$")
    private String password;

    public CreateUserDTO() {
    }

    public CreateUserDTO(String firstName, String lastName, String email, String username, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.username = username;
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
