package com.telerikacademy.forumproject.models.dto;

import javax.validation.constraints.*;

public class UpdateUserDTO {

    @NotEmpty(message = "First name cannot be empty")
    @Size(min = 4, max = 32, message = "First name should be between 4 and 32 characters")
    private String firstName;

    @NotEmpty(message = "Last name cannot be empty")
    @Size(min = 4, max = 32, message = "Last name should be between 4 and 32 characters")
    private String lastName;

    @NotEmpty(message = "Email cannot be empty")
    @Email(message = "Email is not valid", regexp = "^(.+)@(.+)$")
    private String email;

    @NotEmpty(message = "Password cannot be empty")
    @Pattern(message = "Password is not valid", regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&+=])(?=\\S+$).{6,}$")
    private String password;

    @NotEmpty(message = "Password confirmation can't be empty")
    private String confirmPassword;

    private String phoneNumber;

    private String photoUrl;

    public UpdateUserDTO() {
    }

    public UpdateUserDTO(String firstName, String lastName, String email, String password, String confirmPassword, String phoneNumber, String photoUrl) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.confirmPassword = confirmPassword;
        this.phoneNumber = phoneNumber;
        this.photoUrl = photoUrl;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }
}

