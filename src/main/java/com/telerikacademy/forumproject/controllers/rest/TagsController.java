package com.telerikacademy.forumproject.controllers;

import com.telerikacademy.forumproject.exceptions.DuplicateEntityException;
import com.telerikacademy.forumproject.exceptions.EntityNotFoundException;
import com.telerikacademy.forumproject.exceptions.UnauthorizedOperationException;
import com.telerikacademy.forumproject.models.Tags;
import com.telerikacademy.forumproject.models.Users;
import com.telerikacademy.forumproject.models.dto.TagDTO;
import com.telerikacademy.forumproject.services.contracts.TagService;
import com.telerikacademy.forumproject.utils.helpers.AuthenticationHelper;
import com.telerikacademy.forumproject.utils.mappers.TagMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/tags")
public class TagsController {

    private final TagService tagService;
    private final TagMapper tagMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public TagsController(TagService tagService, TagMapper tagMapper, AuthenticationHelper authenticationHelper) {
        this.tagService = tagService;
        this.tagMapper = tagMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<Tags> getAllTags() {
        return tagService.getAllTags();
    }

    @GetMapping("/{id}")
    public Tags getTagById(@PathVariable int id) {
        return tagService.getTagById(id);
    }

    @PostMapping
    public Tags create(@RequestHeader HttpHeaders headers, @Valid @RequestBody TagDTO tagDTO) {
        Users user = authenticationHelper.tryGetUser(headers);
        Tags tag = tagMapper.createDTOToObject(tagDTO);

        try {
            return tagService.create(tag, user);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public void update(@RequestHeader HttpHeaders headers, @RequestBody TagDTO tagDTO, @PathVariable int id) {
        Users user = authenticationHelper.tryGetUser(headers);
        Tags tag = tagMapper.updateDTOToObject(tagDTO, id);

        try {
            tagService.update(tag, user);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        Users user = authenticationHelper.tryGetUser(headers);

        try {
            tagService.delete(id, user);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
