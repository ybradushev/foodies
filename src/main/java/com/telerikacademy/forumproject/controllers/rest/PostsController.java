package com.telerikacademy.forumproject.controllers.rest;

import com.telerikacademy.forumproject.exceptions.DuplicateEntityException;
import com.telerikacademy.forumproject.exceptions.EntityNotFoundException;
import com.telerikacademy.forumproject.exceptions.UnauthorizedOperationException;
import com.telerikacademy.forumproject.models.Comments;
import com.telerikacademy.forumproject.models.Posts;
import com.telerikacademy.forumproject.models.Users;
import com.telerikacademy.forumproject.models.dto.CreateCommentDTO;
import com.telerikacademy.forumproject.models.dto.CreatePostDTO;
import com.telerikacademy.forumproject.models.dto.UpdateCommentDTO;
import com.telerikacademy.forumproject.models.dto.UpdatePostDTO;
import com.telerikacademy.forumproject.services.contracts.CommentsService;
import com.telerikacademy.forumproject.services.contracts.PostsService;
import com.telerikacademy.forumproject.utils.helpers.AuthenticationHelper;
import com.telerikacademy.forumproject.utils.mappers.CommentMapper;
import com.telerikacademy.forumproject.utils.mappers.PostMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/posts")
public class PostsController {
    private final PostsService postsService;
    private final CommentsService commentsService;
    private final PostMapper postMapper;
    private final CommentMapper commentMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public PostsController(PostsService service, CommentsService commentsService, PostMapper mapper, CommentMapper commentMapper, AuthenticationHelper authenticationHelper) {
        this.postsService = service;
        this.commentsService = commentsService;
        this.postMapper = mapper;
        this.commentMapper = commentMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<Posts> getAllPosts(@RequestParam(required = false) Optional<List<String>> tags,
                                   @RequestHeader HttpHeaders headers) {
        if (!headers.containsKey("Authorization")) {
            return postsService.getRecentlyCreatedPosts();
        }

        return postsService.searchPostByTag(tags);
    }

    @GetMapping("/{id}")
    public Posts getPostById(@PathVariable int id) {
        return postsService.getPostById(id);
    }

    @GetMapping("/users/{id}")
    public List<Posts> getPostsByUserId(@PathVariable int id) {
        return postsService.getPostsByAuthor(id);
    }

    @GetMapping("/filter")
    public List<Posts> filter(@RequestParam(required = false) Optional<String> title,
                              @RequestParam(required = false) Optional<Integer> authorId,
                              @RequestParam(required = false) Optional<Integer> categoryId,
                              @RequestParam(required = false) Optional<String> startDate,
                              @RequestParam(required = false) Optional<String> endDate,
                              @RequestParam(required = false) Optional<String> sort) {
        try {
            return postsService.filter(title, authorId, categoryId, startDate, endDate, sort);
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PostMapping
    public Posts createPost(@Valid @RequestBody CreatePostDTO postDTO, @RequestHeader HttpHeaders headers) {
        Users user = authenticationHelper.tryGetUser(headers);
        Posts post = postMapper.createDTOToObject(postDTO);
        try {
            postsService.createPost(post, user);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }

        return post;
    }

    @PutMapping("/{id}")
    public void updatePosts(@Valid @RequestBody UpdatePostDTO postDTO, @PathVariable int id,
                            @RequestHeader HttpHeaders headers) {
        Posts post = postMapper.updateDTOToObject(postDTO, id);
        Users user = authenticationHelper.tryGetUser(headers);
        try {
            postsService.updatePost(post, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PostMapping("/{id}/comments")
    public void replyPost(@Valid @RequestBody CreateCommentDTO commentDTO,
                          @PathVariable int id,
                          @RequestHeader HttpHeaders headers) {
        Users author = authenticationHelper.tryGetUser(headers);
        Comments comment = commentMapper.dtoToObject(commentDTO);

        try {
            postsService.replyPost(comment, author, id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{postId}/comments/{commentId}")
    public void updateComment(@Valid @RequestBody UpdateCommentDTO commentDTO,
                              @PathVariable int postId,
                              @PathVariable int commentId,
                              @RequestHeader HttpHeaders headers) {
        Users operatingUser = authenticationHelper.tryGetUser(headers);
        Comments comment = commentMapper.updateDTOToObject(commentDTO, commentId);
        Posts post = postsService.getPostById(postId);

        try {
            commentsService.updatePost(comment, operatingUser, post);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}/like")
    public void likePost(@PathVariable int id, @RequestHeader HttpHeaders headers) {
        Users operatingUser = authenticationHelper.tryGetUser(headers);

        try {
            postsService.likePost(operatingUser, id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void deletePost(@PathVariable int id, @RequestHeader HttpHeaders headers) {
        Users operatingUser = authenticationHelper.tryGetUser(headers);

        try {
            postsService.deletePost(id, operatingUser);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/liked")
    public List<Posts> getLikedPosts() {
        return postsService.getTopLikedPosts();
    }
}
