package com.telerikacademy.forumproject.controllers;

import com.telerikacademy.forumproject.exceptions.DuplicateEntityException;
import com.telerikacademy.forumproject.exceptions.EntityNotFoundException;
import com.telerikacademy.forumproject.exceptions.UnauthorizedOperationException;
import com.telerikacademy.forumproject.models.Users;
import com.telerikacademy.forumproject.models.dto.RegisterDTO;
import com.telerikacademy.forumproject.models.dto.UpdateUserDTO;
import com.telerikacademy.forumproject.services.contracts.UserService;
import com.telerikacademy.forumproject.utils.helpers.AuthenticationHelper;
import com.telerikacademy.forumproject.utils.helpers.ImageHelper;
import com.telerikacademy.forumproject.utils.mappers.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/users")
public class UsersController {

    private final UserService service;
    private final UserMapper mapper;
    private final AuthenticationHelper authenticationHelper;
    private final ImageHelper imageHelper;

    @Autowired
    public UsersController(UserService service,
                           UserMapper mapper,
                           AuthenticationHelper authenticationHelper,
                           ImageHelper imageHelper) {

        this.service = service;
        this.mapper = mapper;
        this.authenticationHelper = authenticationHelper;
        this.imageHelper = imageHelper;
    }

    @GetMapping
    public List<Users> getAllUsers(@RequestParam(required = false) Optional<String> search,
                                   @RequestHeader HttpHeaders headers) {
        Users operatingUser = authenticationHelper.tryGetUser(headers);
        return service.search(search, operatingUser);
    }

    @GetMapping("/{id}")
    public Users getUserById(@PathVariable int id) {
        try {
            return service.getUserById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Users createUser(@Valid @RequestBody RegisterDTO userDTO) {
        Users user = mapper.createDTOToObject(userDTO);
        try {
            service.createUser(user);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }

        return user;
    }

    @PutMapping("/{id}")
    public void updateUser(@PathVariable int id, @RequestHeader HttpHeaders headers, @Valid @RequestBody UpdateUserDTO userDTO) {
        try {
            Users userToAccess = authenticationHelper.tryGetUser(headers);
            Users userToUpdate = mapper.updateDTOToObject(userDTO, id);
            service.updateUser(userToUpdate, userToAccess);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id, @RequestHeader HttpHeaders headers) {
        Users userToAccess = authenticationHelper.tryGetUser(headers);
        try {
            service.delete(id, userToAccess);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/{id}/save_photo")
    public void savePhoto(@RequestParam(value = "image", required = false) MultipartFile image,
                          @PathVariable int id,
                          @RequestHeader HttpHeaders headers) {
        try {
            Users operatingUser = authenticationHelper.tryGetUser(headers);
            String pictureURL = imageHelper.uploadImage(image);
            service.setProfilePicture(id, pictureURL, operatingUser);
        } catch (IOException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}
