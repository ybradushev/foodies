package com.telerikacademy.forumproject.controllers.rest;

import com.telerikacademy.forumproject.exceptions.DuplicateEntityException;
import com.telerikacademy.forumproject.exceptions.EntityNotFoundException;
import com.telerikacademy.forumproject.exceptions.UnauthorizedOperationException;
import com.telerikacademy.forumproject.models.AdminsInfo;
import com.telerikacademy.forumproject.models.Users;
import com.telerikacademy.forumproject.models.dto.CreateAdminsInfoDTO;
import com.telerikacademy.forumproject.services.contracts.AdminsInfoService;
import com.telerikacademy.forumproject.services.contracts.UserService;
import com.telerikacademy.forumproject.utils.helpers.AuthenticationHelper;
import com.telerikacademy.forumproject.utils.mappers.AdminsInfoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/admin")
public class AdminController {

    private final UserService userService;
    private final AdminsInfoService adminsInfoService;
    private final AuthenticationHelper authenticationHelper;
    private final AdminsInfoMapper adminsInfoMapper;

    @Autowired
    public AdminController(UserService userService, AdminsInfoService adminsInfoService, AuthenticationHelper authenticationHelper, AdminsInfoMapper adminsInfoMapper) {
        this.userService = userService;
        this.adminsInfoService = adminsInfoService;
        this.authenticationHelper = authenticationHelper;
        this.adminsInfoMapper = adminsInfoMapper;
    }

    @PutMapping("/block/{id}")
    public void blockUser(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        Users operatingUser = authenticationHelper.tryGetUser(headers);

        try {
            userService.blockUser(operatingUser, id);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("/unblock/{id}")
    public void unblockUser(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        Users operatingUser = authenticationHelper.tryGetUser(headers);

        try {
            userService.unblockUser(operatingUser, id);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("/promote/{id}")
    public void promoteUser(@RequestHeader HttpHeaders headers, @PathVariable int id, @Valid @RequestBody CreateAdminsInfoDTO createAdminsInfoDTO) {
        Users operatingUser = authenticationHelper.tryGetUser(headers);
        AdminsInfo adminsInfo = adminsInfoMapper.createDTOToObject(createAdminsInfoDTO);

        try {
            adminsInfoService.promoteUser(operatingUser, id, adminsInfo);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
