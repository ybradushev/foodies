package com.telerikacademy.forumproject.controllers.mvc;

import com.telerikacademy.forumproject.exceptions.AuthenticationFailureException;
import com.telerikacademy.forumproject.exceptions.DuplicateEntityException;
import com.telerikacademy.forumproject.exceptions.EntityNotFoundException;
import com.telerikacademy.forumproject.exceptions.UnauthorizedOperationException;
import com.telerikacademy.forumproject.models.Comments;
import com.telerikacademy.forumproject.models.Posts;
import com.telerikacademy.forumproject.models.Users;
import com.telerikacademy.forumproject.models.dto.CreateAdminsInfoDTO;
import com.telerikacademy.forumproject.models.dto.UpdateUserDTO;
import com.telerikacademy.forumproject.services.contracts.PostsService;
import com.telerikacademy.forumproject.services.contracts.UserService;
import com.telerikacademy.forumproject.utils.helpers.AuthenticationHelper;
import com.telerikacademy.forumproject.utils.helpers.ImageHelper;
import com.telerikacademy.forumproject.utils.mappers.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Optional;

@Controller
@RequestMapping("/users")
public class UserPageMvcController {
    private final UserService userService;
    private final PostsService postsService;
    private final ImageHelper imageHelper;
    private final AuthenticationHelper authenticationHelper;
    private final UserMapper userMapper;

    @Autowired
    public UserPageMvcController(UserService userService, PostsService postsService, ImageHelper imageHelper, AuthenticationHelper authenticationHelper, UserMapper userMapper) {
        this.userService = userService;
        this.postsService = postsService;
        this.imageHelper = imageHelper;
        this.authenticationHelper = authenticationHelper;
        this.userMapper = userMapper;
    }

    @GetMapping("/{userId}")
    public String getSingleUser(@PathVariable int userId, HttpSession session, Model model) {
        try {
            authenticationHelper.tryGetUser(session);
            Users user = userService.getUserById(userId);
            model.addAttribute("user", user);
            model.addAttribute("postsByUser", postsService.getPostsByAuthor(user.getUserId()));
            return "UserPage";
        } catch (EntityNotFoundException e) {
            return "Page404Error";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @GetMapping("/{userId}/update")
    public String showUpdateUserPage(@PathVariable int userId, Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            Users user = userService.getUserById(userId);
            UpdateUserDTO userDTO = userMapper.toDto(user);
            model.addAttribute("userId", userId);
            model.addAttribute("user", userDTO);
            model.addAttribute("adminsInfo", new CreateAdminsInfoDTO());
            return "EditProfile";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "Page404Error";
        }
    }

    @PostMapping("/{userId}/update")
    public String updateUser(@PathVariable int userId,
                             @Valid @ModelAttribute("user") UpdateUserDTO dto,
                             @RequestParam Optional<String> phoneNumber,
                             BindingResult errors,
                             Model model,
                             HttpSession session) {
        Users userToAccesss;
        try {
            userToAccesss = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (errors.hasErrors()) {
            System.out.println(errors.getAllErrors());
            return "EditProfile";

        }

        try {
            Users user = userMapper.fromDto(dto, userId);
            userService.updateUser(user, userToAccesss);

            return "redirect:/home";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("name", "duplicate_user", e.getMessage());
            System.out.println(e.getMessage());
            return "EditProfile";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "Page404Error";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "CantAccessPage";
        }
    }

    @GetMapping("/{userId}/posts/{postId}/delete")
    public String deletePost(@PathVariable int userId,
                             @PathVariable int postId,
                             Model model,
                             HttpSession session) {
        Users user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            postsService.deletePost(postId, user);
            return "redirect:/users/" + userId;
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            System.out.println(e.getMessage());
            return "Page404Error";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            System.out.println(e.getMessage());
            return "CantAccessPage";
        }
    }

    @PostMapping("/{userId}/posts/{postId}/update")
    public String updatePost(@PathVariable int userId,
                                @PathVariable int postId,
                                @RequestParam String updateTitle,
                                @RequestParam String updateContent,
                                HttpSession session){
        Users user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try{
            Posts postToUpdate = postsService.getPostById(postId);
            postToUpdate.setPostTitle(updateTitle);
            postToUpdate.setPostContent(updateContent);
            postsService.updatePost(postToUpdate, user);

            return "redirect:/users/" + userId;
        } catch (EntityNotFoundException e){
            return "Page404Error";
        } catch (UnauthorizedOperationException e){
            return "CantAccessPage";
        }
    }

    @PostMapping("/{userId}/save-photo")
    public String savePhoto(@RequestParam MultipartFile file,
                            @PathVariable int userId,
                            HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            Users userToChangePhoto = userService.getUserById(userId);
            String pictureURL = imageHelper.uploadImage(file);
            userService.setProfilePicture(userId, pictureURL, userToChangePhoto);
            return "redirect:/users/" + userId + "/update";
        } catch (IOException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping("/{userId}/block")
    public String blockUser(@PathVariable int userId, HttpSession session) {
        Users operatingUser;
        try {
            operatingUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        userService.blockUser(operatingUser, userId);
        return "redirect:/users/" + userId;
    }

    @PostMapping("/{userId}/unblock")
    public String unblockUser(@PathVariable int userId, HttpSession session) {
        Users operatingUser;
        try {
            operatingUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        userService.unblockUser(operatingUser, userId);
        return "redirect:/users/" + userId;
    }

    @PostMapping("/{userId}/delete")
    public String deleteUser(@PathVariable int userId, HttpSession session){
        Users operatingUser;
        try {
            operatingUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        userService.delete(userId, operatingUser);
        return "redirect:/";
    }
}
