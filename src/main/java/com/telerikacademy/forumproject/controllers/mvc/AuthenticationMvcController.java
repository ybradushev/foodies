package com.telerikacademy.forumproject.controllers.mvc;

import com.telerikacademy.forumproject.exceptions.AuthenticationFailureException;
import com.telerikacademy.forumproject.exceptions.DuplicateEmailException;
import com.telerikacademy.forumproject.exceptions.DuplicateEntityException;
import com.telerikacademy.forumproject.exceptions.DuplicateUsernameException;
import com.telerikacademy.forumproject.models.Users;
import com.telerikacademy.forumproject.models.dto.LoginDTO;
import com.telerikacademy.forumproject.models.dto.RegisterDTO;
import com.telerikacademy.forumproject.services.contracts.UserService;
import com.telerikacademy.forumproject.utils.helpers.AuthenticationHelper;
import com.telerikacademy.forumproject.utils.mappers.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping("/auth")
public class AuthenticationMvcController {
    private final UserService userService;
    private final UserMapper userMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public AuthenticationMvcController(UserService userService, UserMapper userMapper, AuthenticationHelper authenticationHelper) {
        this.userService = userService;
        this.userMapper = userMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping("/login")
    public String showLoginPage(Model model) {
        model.addAttribute("login", new LoginDTO());
        return "Login";
    }

    @PostMapping("/login")
    public String handleLogin(@Valid @ModelAttribute("login") LoginDTO login,
                              BindingResult bindingResult,
                              HttpSession session) {
        if (bindingResult.hasErrors()) {
            return "Login";
        }

        try {
            authenticationHelper.verifyAuthentication(login.getUsername(), login.getPassword());
            session.setAttribute("currentUser", userService.getUserByUsername(login.getUsername()));
            return "redirect:/home";
        } catch (AuthenticationFailureException e) {
            bindingResult.rejectValue("username", "auth_error", e.getMessage());
            return "Login";
        }
    }

    @GetMapping("/logout")
    public String handleLogout(HttpSession session) {
        session.removeAttribute("currentUser");
        return "redirect:/";
    }

    @GetMapping("/register")
    public String showRegisterPage(Model model) {
        model.addAttribute("register", new RegisterDTO());
        return "Register";
    }

    @PostMapping("/register")
    public String handleRegister(@Valid @ModelAttribute("register") RegisterDTO register,
                                 BindingResult bindingResult,
                                 HttpSession session) {
        if (bindingResult.hasErrors()) {
            return "Register";
        }

        if (!register.getPassword().equals(register.getConfirmPassword())) {
            bindingResult.rejectValue("passwordConfirm", "password_error", "Password confirmation should match password.");
            return "Register";
        }

        try {
            Users user = userMapper.createDTOToObject(register);
            userService.createUser(user);
            return "redirect:/auth/login";
        } catch (DuplicateEmailException e) {
            bindingResult.rejectValue("email", "email_error", e.getMessage());
            return "Register";
        } catch (DuplicateUsernameException e) {
            bindingResult.rejectValue("username", "username_error", e.getMessage());
            return "Register";
        }
    }
}
