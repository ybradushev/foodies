package com.telerikacademy.forumproject.controllers.mvc;

import com.telerikacademy.forumproject.exceptions.AuthenticationFailureException;
import com.telerikacademy.forumproject.exceptions.EntityNotFoundException;
import com.telerikacademy.forumproject.exceptions.UnauthorizedOperationException;
import com.telerikacademy.forumproject.models.Categories;
import com.telerikacademy.forumproject.models.Posts;
import com.telerikacademy.forumproject.models.Users;
import com.telerikacademy.forumproject.services.contracts.CategoryService;
import com.telerikacademy.forumproject.services.contracts.PostsService;
import com.telerikacademy.forumproject.utils.helpers.AuthenticationHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.Optional;

@RequestMapping("/categories")
@Controller
public class CategoryPageMvcController {
    private final CategoryService categoryService;
    private final PostsService postsService;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public CategoryPageMvcController(CategoryService categoryService, PostsService postsService, AuthenticationHelper authenticationHelper) {
        this.categoryService = categoryService;
        this.postsService = postsService;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping("/{categoryId}")
    public String showCategoryPage(@PathVariable int categoryId, Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
            Categories category = categoryService.getCategoryById(categoryId);
            model.addAttribute("category", category);
            model.addAttribute("postsByCategory", postsService.getPostsByCategory(category));
            return "CategoryPage";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "Page404Error";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @GetMapping("/{categoryId}/posts/{postId}/delete")
    public String deletePost(@PathVariable int categoryId, @PathVariable int postId, Model model, HttpSession session) {
        Users user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            postsService.deletePost(postId, user);
            return "redirect:/categories/" + categoryId;
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            System.out.println(e.getMessage());
            return "Page404Error";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            System.out.println(e.getMessage());
            return "CantAccessPage";
        }
    }

    @PostMapping("/{categoryId}/posts/{postId}/update")
    public String updatePost(@PathVariable int categoryId,
                             @PathVariable int postId,
                             @RequestParam String updateTitle,
                             @RequestParam String updateContent,
                             HttpSession session){
        Users user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try{
            Posts postToUpdate = postsService.getPostById(postId);
            postToUpdate.setPostTitle(updateTitle);
            postToUpdate.setPostContent(updateContent);
            postsService.updatePost(postToUpdate, user);

            return "redirect:/categories/" + categoryId;
        } catch (EntityNotFoundException e){
            return "Page404Error";
        } catch (UnauthorizedOperationException e){
            return "CantAccessPage";
        }
    }

    @GetMapping("{categoryId}/posts/filter")
    public String filterPosts(Model model, HttpSession session, @PathVariable int categoryId,
                              @RequestParam(required = false) Optional<String> title,
                              @RequestParam(required = false) Optional<String> authorUsername,
                              @RequestParam(required = false) Optional<String> startDate,
                              @RequestParam(required = false) Optional<String> endDate,
                              @RequestParam(required = false) Optional<String> sort
    ) {
        Users user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            Categories category = categoryService.getCategoryById(categoryId);
            model.addAttribute("category", category);
            model.addAttribute("postsByCategory", postsService.updatedFilter(title, authorUsername, Optional.of(categoryId), startDate, endDate, sort));
            return "CategoryPage";
        } catch (EntityNotFoundException e) {
            return "Page404Error";
        }
    }
}
