package com.telerikacademy.forumproject.controllers.mvc;

import com.telerikacademy.forumproject.exceptions.AuthenticationFailureException;
import com.telerikacademy.forumproject.models.Categories;
import com.telerikacademy.forumproject.models.Posts;
import com.telerikacademy.forumproject.services.contracts.CategoryService;
import com.telerikacademy.forumproject.services.contracts.PostsService;
import com.telerikacademy.forumproject.utils.helpers.AuthenticationHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("/home")
public class HomeMvcController {

    private final PostsService postsService;
    private final CategoryService categoryService;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public HomeMvcController(PostsService postsService, CategoryService categoryService, AuthenticationHelper authenticationHelper) {
        this.postsService = postsService;
        this.categoryService = categoryService;
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("recentlyCreated")
    public List<Posts> getRecentlyCreatedPosts() {
        return postsService.getRecentlyCreatedPosts();
    }

    @ModelAttribute("mostCommented")
    public List<Posts> getMostCommentedPosts() {
        return postsService.getMostCommentedPosts();
    }

    @ModelAttribute("categories")
    public List<Categories> getAllCategories() {
        return categoryService.getAllCategories();
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @GetMapping
    public String showHomePage(Model model, HttpSession session){
        try{
            authenticationHelper.tryGetUser(session);
            return "Home";
        } catch (AuthenticationFailureException e){
            return "redirect:/auth/login";
        }

    }

    @GetMapping("/postId")
    public String showSinglePost() {
        return "ViewPost";
    }
}
