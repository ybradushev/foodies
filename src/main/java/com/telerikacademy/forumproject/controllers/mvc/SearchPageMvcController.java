package com.telerikacademy.forumproject.controllers.mvc;

import com.telerikacademy.forumproject.exceptions.AuthenticationFailureException;
import com.telerikacademy.forumproject.services.contracts.PostsService;
import com.telerikacademy.forumproject.utils.helpers.AuthenticationHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.Optional;

@RequestMapping("/search")
@Controller
public class SearchPageMvcController {
    private final AuthenticationHelper authenticationHelper;
    private final PostsService postsService;

    @Autowired
    public SearchPageMvcController(AuthenticationHelper authenticationHelper, PostsService postsService) {
        this.authenticationHelper = authenticationHelper;
        this.postsService = postsService;
    }

    @GetMapping
    public String searchNavBar(@RequestParam Optional<String> keyword, Model model, HttpSession session){
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        model.addAttribute("foundResults", postsService.filter(keyword, Optional.empty(),
                Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty()));

        return "SearchPage";
    }
}
