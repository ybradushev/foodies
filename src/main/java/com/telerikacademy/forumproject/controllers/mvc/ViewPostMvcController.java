package com.telerikacademy.forumproject.controllers.mvc;

import com.telerikacademy.forumproject.exceptions.AuthenticationFailureException;
import com.telerikacademy.forumproject.exceptions.EntityNotFoundException;
import com.telerikacademy.forumproject.exceptions.UnauthorizedOperationException;
import com.telerikacademy.forumproject.models.Comments;
import com.telerikacademy.forumproject.models.Posts;
import com.telerikacademy.forumproject.models.Users;
import com.telerikacademy.forumproject.models.dto.CreateCommentDTO;
import com.telerikacademy.forumproject.services.contracts.CommentsService;
import com.telerikacademy.forumproject.services.contracts.PostsService;
import com.telerikacademy.forumproject.utils.helpers.AuthenticationHelper;
import com.telerikacademy.forumproject.utils.mappers.CommentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.Optional;

@RequestMapping("/posts")
@Controller
public class ViewPostMvcController {
    private final AuthenticationHelper authenticationHelper;
    private final PostsService postsService;
    private final CommentsService commentsService;
    private final CommentMapper commentMapper;

    @Autowired
    public ViewPostMvcController(AuthenticationHelper authenticationHelper, PostsService postsService, CommentsService commentsService, CommentMapper commentMapper) {
        this.authenticationHelper = authenticationHelper;
        this.postsService = postsService;
        this.commentsService = commentsService;
        this.commentMapper = commentMapper;
    }

    @GetMapping("/{postId}")
    public String showSinglePost(@PathVariable int postId, HttpSession session, Model model) {
        try {
            authenticationHelper.tryGetUser(session);
            Posts post = postsService.getPostById(postId);
            model.addAttribute("post", post);
            model.addAttribute("reply", new CreateCommentDTO());

            return "ViewPost";
        } catch (EntityNotFoundException e) {
            return "Page404Error";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @GetMapping("/reply-modal")
    public String showReplyModal() {
        return "ViewPost1";
    }

    @GetMapping("/{postId}/comments/{commentId}/delete")
    public String deleteComment(@PathVariable int postId, @PathVariable int commentId, Model model, HttpSession session) {
        Users user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            commentsService.deleteComment(commentId, user);
            return "redirect:/posts/" + postId;
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            System.out.println(e.getMessage());
            return "Page404Error";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            System.out.println(e.getMessage());
            return "CantAccessPage";
        }
    }

    @PostMapping("/{postId}/reply")
    public String addComment(@Valid @ModelAttribute("reply") CreateCommentDTO reply,
                             @PathVariable int postId,
                             BindingResult bindingResult,
                             Model model,
                             HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (bindingResult.hasErrors()) {
            return "redirect:/posts/" + postId;
        }

        try {
            Comments comment = commentMapper.dtoToObject(reply);
            commentsService.addCommentToPost(comment);
            return "redirect:/posts/" + postId;
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "CantAccessPage";
        }
    }

    @PostMapping("/{postId}/comments/{commentId}/update")
    public String updateComment(@PathVariable int postId,
                                @PathVariable int commentId,
                                @RequestParam String updateContent,
                                HttpSession session){
        Users user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try{
            Comments commentToUpdate = commentsService.getCommentById(commentId);
            Posts post = postsService.getPostById(postId);
            commentToUpdate.setCommentContent(updateContent);
            commentsService.updatePost(commentToUpdate, user, post);

            return "redirect:/posts/" + postId;
        } catch (EntityNotFoundException e){
            return "Page404Error";
        } catch (UnauthorizedOperationException e){
            return "CantAccessPage";
        }
    }

    @PostMapping("/{postId}/update")
    public String updatePost(@PathVariable int postId,
                             @RequestParam String updateTitle,
                             @RequestParam String updateContent,
                             HttpSession session){
        Users user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try{
            Posts postToUpdate = postsService.getPostById(postId);
            postToUpdate.setPostTitle(updateTitle);
            postToUpdate.setPostContent(updateContent);
            postsService.updatePost(postToUpdate, user);

            return "redirect:/posts/" + postId;
        } catch (EntityNotFoundException e){
            return "Page404Error";
        } catch (UnauthorizedOperationException e){
            return "CantAccessPage";
        }
    }

    @GetMapping("/{postId}/delete")
    public String deletePost(@PathVariable int postId, Model model, HttpSession session) {
        Users user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            postsService.deletePost(postId, user);
            return "redirect:/home";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            System.out.println(e.getMessage());
            return "Page404Error";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            System.out.println(e.getMessage());
            return "CantAccessPage";
        }
    }

    @PostMapping("/{postId}/user/{userId}/like")
    public String likePost(@PathVariable int postId, @PathVariable int userId, HttpSession session){
        Users user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try{
            postsService.likePost(user, postId);
            return "redirect:/posts/" + postId;
        } catch (EntityNotFoundException e){
            return "Page404Error";
        }
    }

    @PostMapping("/{postId}/user/{userId}/unlike")
    public String unlikePost(@PathVariable int postId, @PathVariable int userId, HttpSession session){
        Users user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try{
            postsService.unlikePost(user, postId);
            return "redirect:/posts/" + postId;
        } catch (EntityNotFoundException e){
            return "Page404Error";
        }
    }
}
