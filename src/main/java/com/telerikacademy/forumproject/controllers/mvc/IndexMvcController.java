package com.telerikacademy.forumproject.controllers.mvc;

import com.telerikacademy.forumproject.models.Posts;
import com.telerikacademy.forumproject.services.contracts.CommentsService;
import com.telerikacademy.forumproject.services.contracts.PostsService;
import com.telerikacademy.forumproject.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/")
public class IndexMvcController {

    private final UserService userService;
    private final PostsService postsService;
    private final CommentsService commentsService;

    @Autowired
    public IndexMvcController(UserService userService, PostsService postsService, CommentsService commentsService) {
        this.userService = userService;
        this.postsService = postsService;
        this.commentsService = commentsService;
    }

    @ModelAttribute("postsCount")
    public int getPostsCount() {
        return postsService.getAllPosts().size();
    }

    @ModelAttribute("usersCount")
    public int getUsersCount() {
        return userService.getAllUsers().size();
    }

    @ModelAttribute("commentsCount")
    public int getCommentsCount() {
        return commentsService.getAllComments().size();
    }

    @GetMapping
    public String showIndexPage(Model model) {
        return "index";
    }

    @ModelAttribute("recentlyCreated")
    public List<Posts> getRecentlyCreatedPosts(){
        return postsService.getRecentlyCreatedPosts();
    }

    @ModelAttribute("mostCommented")
    public List<Posts> getMostCommentedPosts(){
        return postsService.getMostCommentedPosts();
    }
}
