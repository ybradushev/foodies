package com.telerikacademy.forumproject.services;

import com.telerikacademy.forumproject.exceptions.DuplicateEntityException;
import com.telerikacademy.forumproject.exceptions.EntityNotFoundException;
import com.telerikacademy.forumproject.exceptions.UnauthorizedOperationException;
import com.telerikacademy.forumproject.models.Categories;
import com.telerikacademy.forumproject.models.Comments;
import com.telerikacademy.forumproject.models.Posts;
import com.telerikacademy.forumproject.models.Users;
import com.telerikacademy.forumproject.repositories.contracts.CommentsRepository;
import com.telerikacademy.forumproject.repositories.contracts.PostsRepository;
import com.telerikacademy.forumproject.repositories.contracts.TagRepository;
import com.telerikacademy.forumproject.repositories.contracts.UserRepository;
import com.telerikacademy.forumproject.services.contracts.PostsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static com.telerikacademy.forumproject.services.CommentsServiceImpl.DEFAULT_USER_ID;

@Service
public class PostsServiceImpl implements PostsService {
    private final PostsRepository postsRepository;
    private final UserRepository userRepository;
    private final TagRepository tagRepository;
    private final CommentsRepository commentsRepository;

    @Autowired
    public PostsServiceImpl(PostsRepository repository, UserRepository userRepository,
                            TagRepository tagRepository, CommentsRepository commentsRepository) {
        this.postsRepository = repository;
        this.userRepository = userRepository;
        this.tagRepository = tagRepository;
        this.commentsRepository = commentsRepository;
    }

    @Override
    public List<Posts> getAllPosts() {
        return postsRepository.getAllPosts();
    }

    @Override
    public Posts getPostById(int id) {
        return postsRepository.getPostById(id);
    }

    public List<Posts> getPostsByCategory(Categories category){
        return postsRepository.getPostsByCategory(category);
    }



    @Override
    public Posts createPost(Posts post, Users user) {
        if (user.isUserBlocked()) {
            throw new UnauthorizedOperationException("Blocked users cannot create new posts");
        }

        boolean duplicateExists = true;
        try {
            postsRepository.getPostById(post.getPostId());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Post", "id", String.valueOf(post.getPostId()));
        }

        return postsRepository.createPost(post);
    }

    @Override
    public void updatePost(Posts post, Users user) {
        if (user.isUserBlocked()) {
            throw new UnauthorizedOperationException("Blocked users cannot update posts");
        }

        if (!post.getPostAuthor().equals(user)) {
            throw new UnauthorizedOperationException("You can only edit your own posts");
        }

//        boolean duplicateExists = true;
//        try {
//            Posts existingPost = postsRepository.getPostById(post.getPostId());
//            if (!existingPost.getPostTitle().equals(post.getPostTitle())) {
//                duplicateExists = false;
//            }
//        } catch (EntityNotFoundException e) {
//            duplicateExists = false;
//        }
//
//        if (duplicateExists) {
//            throw new DuplicateEntityException("Post", "title", post.getPostTitle());
//        }

        postsRepository.updatePost(post);
    }

    @Override
    public void deletePost(int id, Users operatingUser) {
        String userRole = operatingUser.getUserRole().getUserTypeName();

        if (userRole.equals("User") && !getPostById(id).getPostAuthor().equals(operatingUser) ||
                !userRole.equals("Admin") && !getPostById(id).getPostAuthor().equals(operatingUser)) {
            throw new UnauthorizedOperationException("Only admins and authors of posts can delete posts");
        }

        postsRepository.deleteLikesOfPost(id);
        tagRepository.deleteTagsOfPost(id);
        commentsRepository.deleteCommentsOfPost(id);

        postsRepository.deletePost(id);
    }

    public List<Posts> getPostsByAuthor(int userId) {
        return postsRepository.getPostByAuthor(userId);
    }

    public void makePostsAuthorDefault(int userId) {
        List<Posts> postsByAuthor = getPostsByAuthor(userId);
        Users defaultUser = userRepository.getUserById(DEFAULT_USER_ID);

        postsByAuthor.forEach(post -> post.setPostAuthor(defaultUser));

        postsRepository.makePostWithDefaultUser(postsByAuthor);
    }

    public void deleteLikesByAuthor(int userId) {
        List<Posts> posts = getPostsByLikesList(userId);
        postsRepository.deleteLikesOfUser(posts, userId);
    }

    @Override
    public void replyPost(Comments comment, Users author, int id) {
        if (author.isUserBlocked()) {
            throw new UnauthorizedOperationException("Blocked users cannot reply to posts");
        }

        postsRepository.replyPost(comment, author, id);
    }

    @Override
    public void likePost(Users operatingUser, int id) {
        if (operatingUser.isUserBlocked()) {
            throw new UnauthorizedOperationException("Blocked users cannot like posts");
        }

        Posts postToLike = getPostById(id);

        if (postToLike.getLikesList().contains(operatingUser)) {
            throw new UnauthorizedOperationException("You have already liked this post");
        }

        postsRepository.likePost(operatingUser, postToLike);
    }

    @Override
    public void unlikePost(Users operatingUser, int postId) {
        if (operatingUser.isUserBlocked()) {
            throw new UnauthorizedOperationException("Blocked users cannot like posts");
        }

        Posts postToLike = getPostById(postId);

        if (!postToLike.getLikesList().contains(operatingUser)) {
            throw new UnauthorizedOperationException("You have not liked this post");
        }

        postsRepository.unlikePost(operatingUser, postToLike);
    }

    @Override
    public List<Posts> searchPostByTag(Optional<List<String>> tags) {
        return postsRepository.searchPostByTag(tags);
    }

    @Override
    public List<Posts> filter(Optional<String> title, Optional<Integer> authorId, Optional<Integer> categoryId, Optional<String> startDate, Optional<String> endDate, Optional<String> sort) {
        return postsRepository.filter(title, authorId, categoryId, startDate, endDate, sort);
    }

    @Override
    public List<Posts> updatedFilter(Optional<String> title, Optional<String> authorUsername, Optional<Integer> categoryId, Optional<String> startDate, Optional<String> endDate, Optional<String> sort){
        return postsRepository.updatedFilter(title, authorUsername, categoryId, startDate, endDate, sort);
    }

    public List<Posts> getRecentlyCreatedPosts() {
        return postsRepository.getRecentlyCreatedPosts();
    }

    @Override
    public List<Posts> getTopLikedPosts() {
        return postsRepository.getTopLikedPosts();
    }

    public List<Posts> getPostsByLikesList(int userId) {
        return postsRepository.getPostsByLikesList(userId);
    }

    public List<Posts> getMostCommentedPosts() {
        return postsRepository.getTopMostCommentedPosts();
    }
}
