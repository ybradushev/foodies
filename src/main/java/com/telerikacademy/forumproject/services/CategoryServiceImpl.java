package com.telerikacademy.forumproject.services;

import com.telerikacademy.forumproject.models.Categories;
import com.telerikacademy.forumproject.repositories.contracts.CategoryRepository;
import com.telerikacademy.forumproject.services.contracts.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepository categoryRepository;

    @Autowired
    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<Categories> getAllCategories() {
        return categoryRepository.getAllCategories();
    }

    @Override
    public Categories getCategoryById(int categoryId) {
        return categoryRepository.getCategoryById(categoryId);
    }
}
