package com.telerikacademy.forumproject.services;

import com.telerikacademy.forumproject.exceptions.DuplicateEntityException;
import com.telerikacademy.forumproject.exceptions.EntityNotFoundException;
import com.telerikacademy.forumproject.exceptions.UnauthorizedOperationException;
import com.telerikacademy.forumproject.models.Comments;
import com.telerikacademy.forumproject.models.Posts;
import com.telerikacademy.forumproject.models.Users;
import com.telerikacademy.forumproject.repositories.contracts.CommentsRepository;
import com.telerikacademy.forumproject.repositories.contracts.UserRepository;
import com.telerikacademy.forumproject.services.contracts.CommentsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentsServiceImpl implements CommentsService {
    public static final int DEFAULT_USER_ID = 777;

    private final CommentsRepository commentsRepository;
    private final UserRepository userRepository;

    @Autowired
    public CommentsServiceImpl(CommentsRepository commentsRepository, UserRepository userRepository) {
        this.commentsRepository = commentsRepository;
        this.userRepository = userRepository;
    }

    public List<Comments> getAllComments() {
        return commentsRepository.getAllComments();
    }

    @Override
    public Comments getCommentById(int commentId) {
        return commentsRepository.getCommentById(commentId);
    }

    @Override
    public List<Comments> getCommentsOfPost(int postId) {
        return commentsRepository.getCommentOfPost(postId);
    }

    @Override
    public void addCommentToPost(Comments comment) {
        commentsRepository.addCommentToPost(comment);
    }

    @Override
    public void setDefaultUserToComments(int userId) {
        List<Comments> commentsByAuthor = getCommentsByAuthor(userId);
        Users defaultUser = userRepository.getUserById(DEFAULT_USER_ID);

        commentsByAuthor.forEach(comments -> comments.setCommentAuthor(defaultUser));

        commentsRepository.setDefaultUserToComments(commentsByAuthor);
    }

    @Override
    public List<Comments> getCommentsByAuthor(int userId) {
        return commentsRepository.getCommentsByAuthor(userId);
    }

    @Override
    public void updatePost(Comments comment, Users operatingUser, Posts post) {
        if (operatingUser.isUserBlocked()) {
            throw new UnauthorizedOperationException("Blocked users cannot update posts");
        }

        if (!comment.getCommentAuthor().equals(operatingUser)) {
            throw new UnauthorizedOperationException("You can only edit your own comments");
        }

//        boolean duplicateExists = true;
//        try {
//            Comments existingComment = commentsRepository.getCommentById(comment.getCommentId());
//            if (!existingComment.getCommentContent().equals(comment.getCommentContent())) {
//                duplicateExists = false;
//            }
//        } catch (EntityNotFoundException e) {
//            duplicateExists = false;
//        }
//
//        if (duplicateExists) {
//            throw new DuplicateEntityException("Comment", "content", post.getPostContent());
//        }

        commentsRepository.updateComment(comment);
    }

    public void deleteComment(int commentId, Users userToAccess) {
        if (!getCommentById(commentId).getCommentAuthor().equals(userToAccess)) {
            throw new UnauthorizedOperationException("You can only edit your own comments");
        }

        commentsRepository.deleteComment(commentId);
    }
}
