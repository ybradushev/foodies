package com.telerikacademy.forumproject.services;

import com.telerikacademy.forumproject.exceptions.DuplicateEntityException;
import com.telerikacademy.forumproject.exceptions.UnauthorizedOperationException;
import com.telerikacademy.forumproject.models.AdminsInfo;
import com.telerikacademy.forumproject.models.Users;
import com.telerikacademy.forumproject.repositories.contracts.AdminsInfoRepository;
import com.telerikacademy.forumproject.services.contracts.AdminsInfoService;
import com.telerikacademy.forumproject.services.contracts.UserService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdminsInfoServiceImpl implements AdminsInfoService {

    private final AdminsInfoRepository adminsInfoRepository;
    private final UserService userService;

    public AdminsInfoServiceImpl(AdminsInfoRepository adminsInfoRepository, UserService userService) {
        this.adminsInfoRepository = adminsInfoRepository;
        this.userService = userService;
    }

    @Override
    public List<AdminsInfo> getAllAdminsInfo() {
        return adminsInfoRepository.getAllAdminsInfo();
    }

    @Override
    public void createAdminsInfo(AdminsInfo adminsInfo) {
        adminsInfoRepository.createAdmin(adminsInfo);
    }

    @Override
    public void promoteUser(Users operatingUser, int id, AdminsInfo adminsInfo) {

        if (!userService.isAdmin(operatingUser)) {
            throw new UnauthorizedOperationException("Only admins can promote other users");
        }

        if (userService.isAdmin(userService.getUserById(id))) {
            throw new DuplicateEntityException("User", "role", "Admin");
        }

        AdminsInfo createdAdminsInfo = adminsInfoRepository.createAdmin(adminsInfo);
        Users user = userService.getUserById(id);

        adminsInfoRepository.promoteUser(createdAdminsInfo, user);
    }
}
