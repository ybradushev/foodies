package com.telerikacademy.forumproject.services;

import com.telerikacademy.forumproject.models.UserRole;
import com.telerikacademy.forumproject.repositories.contracts.UserRolesRepository;
import com.telerikacademy.forumproject.services.contracts.UserRolesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserRolesServiceImpl implements UserRolesService {
    private final UserRolesRepository userRolesRepository;

    @Autowired
    public UserRolesServiceImpl(UserRolesRepository userRolesRepository) {
        this.userRolesRepository = userRolesRepository;
    }

    @Override
    public UserRole getRoleById(int id) {
        return userRolesRepository.getRoleById(id);
    }

    @Override
    public UserRole addRole(UserRole userRole) {
        return userRolesRepository.addRole(userRole);
    }
}
