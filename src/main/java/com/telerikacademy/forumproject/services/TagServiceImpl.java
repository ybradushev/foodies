package com.telerikacademy.forumproject.services;

import com.telerikacademy.forumproject.exceptions.DuplicateEntityException;
import com.telerikacademy.forumproject.exceptions.EntityNotFoundException;
import com.telerikacademy.forumproject.exceptions.UnauthorizedOperationException;
import com.telerikacademy.forumproject.models.Tags;
import com.telerikacademy.forumproject.models.Users;
import com.telerikacademy.forumproject.repositories.contracts.TagRepository;
import com.telerikacademy.forumproject.services.contracts.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TagServiceImpl implements TagService {

    private final TagRepository tagRepository;

    @Autowired
    public TagServiceImpl(TagRepository tagRepository) {
        this.tagRepository = tagRepository;
    }

    @Override
    public List<Tags> getAllTags() {
        return tagRepository.getAllTags();
    }

    @Override
    public Tags getTagById(int tagId) {
        return tagRepository.getTagById(tagId);
    }

    @Override
    public Tags create(Tags tag, Users user) {
        if (user.isUserBlocked()) {
            throw new UnauthorizedOperationException("Blocked users cannot create tags");
        }

        boolean duplicateExists = true;
        try {
            tagRepository.getTagByName(tag.getTagName());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Tag", "name", tag.getTagName());
        }

        return tagRepository.create(tag);
    }

    @Override
    public void update(Tags tag, Users user) {
        if (user.isUserBlocked()) {
            throw new UnauthorizedOperationException("Blocked users cannot update tags");
        }

        boolean duplicateExists = true;
        try {
            tagRepository.getTagByName(tag.getTagName());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Tag", "name", tag.getTagName());
        }

        tagRepository.update(tag);
    }

    @Override
    public void delete(int id, Users user) {
        if (user.isUserBlocked()) {
            throw new UnauthorizedOperationException("Blocked users cannot update tags");
        }

        Tags tagToDelete = tagRepository.getTagById(id);

        tagRepository.deleteTagsList(tagToDelete);

        tagRepository.deleteTag(tagToDelete);
    }
}
