package com.telerikacademy.forumproject.services;

import com.telerikacademy.forumproject.exceptions.*;
import com.telerikacademy.forumproject.models.Users;
import com.telerikacademy.forumproject.repositories.contracts.UserRepository;
import com.telerikacademy.forumproject.services.contracts.CommentsService;
import com.telerikacademy.forumproject.services.contracts.PostsService;
import com.telerikacademy.forumproject.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final PostsService postsService;
    private final CommentsService commentsService;

    @Autowired
    public UserServiceImpl(UserRepository repository, PostsService service, CommentsService commentsService) {
        this.userRepository = repository;
        this.postsService = service;
        this.commentsService = commentsService;
    }

    @Override
    public List<Users> getAllUsers() {
        return userRepository.getAllUsers();
    }

    @Override
    public Users getUserById(int id) {
        return userRepository.getUserById(id);
    }

    @Override
    public Users getUserByUsername(String name) {
        return userRepository.getUserByUsername(name);
    }

    @Override
    public void createUser(Users user) {
        boolean usernameExists = true;
        boolean emailExists = true;
        try {
            userRepository.getUserByUsername(user.getUsername());
        } catch (EntityNotFoundException e) {
            usernameExists = false;
        }

        try{
            userRepository.getUserByEmail(user.getEmail());
        } catch (EntityNotFoundException ex){
            emailExists = false;
        }

        if (usernameExists) {
            throw new DuplicateUsernameException("User", "username", String.valueOf(user.getUsername()));
        }

        if (emailExists) {
            throw new DuplicateEmailException("User", "email", String.valueOf(user.getEmail()));
        }

        userRepository.createUser(user);
    }


    //TODO All of this seems wrong and code works the same without it
    @Override
    public void updateUser(Users userToUpdate, Users userToAccess) {
        if (!userToUpdate.equals(userToAccess)) {
            throw new UnauthorizedOperationException("You can only update your own profile");
        }

        if (!userToAccess.getUsername().equals(userToUpdate.getUsername())) {
            throw new UnauthorizedOperationException("You cannot change username");
        }

        userRepository.updateUser(userToUpdate);
    }

    @Override
    public void delete(int userId, Users userToAccess) {
        if (userToAccess.getUserId() != userId) {
            throw new UnauthorizedOperationException("You can only delete your own user");
        }

        postsService.deleteLikesByAuthor(userId);
        commentsService.setDefaultUserToComments(userId);
        postsService.makePostsAuthorDefault(userId);

        userRepository.deleteUser(userId);
    }

    @Override
    public void blockUser(Users operatingUser, int id) {
        if (!operatingUser.getUserRole().getUserTypeName().equalsIgnoreCase("Admin")) {
            throw new UnauthorizedOperationException("Only admins can block users!");
        }

        if (getUserById(id).getUserRole().getUserTypeName().equals("Admin")) {
            throw new UnauthorizedOperationException("Admins cannot be blocked");
        }

        if (getUserById(id).isUserBlocked()) {
            throw new DuplicateEntityException("User", "block status", "true");
        }

        userRepository.blockUser(id);
    }

    @Override
    public void unblockUser(Users operatingUser, int id) {
        if (!isAdmin(operatingUser)) {
            throw new UnauthorizedOperationException("Only admins can unblock users!");
        }

        if (!getUserById(id).isUserBlocked()) {
            throw new DuplicateEntityException("User", "block status", "false");
        }

        userRepository.unblockUser(id);
    }

    public List<Users> search(Optional<String> search, Users operatingUser) {
        if (isAdmin(operatingUser)) {
            return userRepository.search(search);
        }

        return userRepository.getAllUsers();
    }

    @Override
    public boolean isAdmin(Users user) {
        return user.getUserRole().getUserTypeName().equalsIgnoreCase("Admin");
    }

    @Override
    public void setProfilePicture(int userId, String pictureURL, Users operatingUser) {
        if (!getUserById(userId).equals(operatingUser)) {
            throw new UnauthorizedOperationException("You can change only your profile photo");
        }

        userRepository.setProfilePicture(userId, pictureURL);
    }
}
