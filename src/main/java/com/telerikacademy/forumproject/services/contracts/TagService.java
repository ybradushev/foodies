package com.telerikacademy.forumproject.services.contracts;

import com.telerikacademy.forumproject.models.Tags;
import com.telerikacademy.forumproject.models.Users;

import java.util.List;

public interface TagService {
    List<Tags> getAllTags();

    Tags getTagById(int tagId);

    Tags create(Tags tag, Users user);

    void update(Tags tag, Users user);

    void delete(int id, Users user);
}
