package com.telerikacademy.forumproject.services.contracts;

import com.telerikacademy.forumproject.models.Categories;
import com.telerikacademy.forumproject.models.Comments;
import com.telerikacademy.forumproject.models.Posts;
import com.telerikacademy.forumproject.models.Users;

import java.util.List;
import java.util.Optional;

public interface PostsService {
    List<Posts> getAllPosts();

    Posts getPostById(int id);

    Posts createPost(Posts post, Users user);

    void updatePost(Posts post, Users user);

    void deletePost(int id, Users operatingUser);

    List<Posts> getPostsByAuthor(int userId);

    void makePostsAuthorDefault(int userId);

    List<Posts> getPostsByLikesList(int userId);

    void deleteLikesByAuthor(int userId);

    void replyPost(Comments comment, Users author, int id);

    void likePost(Users operatingUser, int id);

    List<Posts> searchPostByTag(Optional<List<String>> tags);

    List<Posts> filter(Optional<String> title, Optional<Integer> authorId, Optional<Integer> categoryId, Optional<String> startDate, Optional<String> endDate, Optional<String> sort);

    List<Posts> updatedFilter(Optional<String> title, Optional<String> authorUsername, Optional<Integer> categoryId, Optional<String> startDate, Optional<String> endDate, Optional<String> sort);

    List<Posts> getRecentlyCreatedPosts();

    List<Posts> getTopLikedPosts();

    List<Posts> getMostCommentedPosts();

    List<Posts> getPostsByCategory(Categories category);

    void unlikePost(Users user, int postId);
}
