package com.telerikacademy.forumproject.services.contracts;

import com.telerikacademy.forumproject.models.UserRole;

public interface UserRolesService {
    UserRole getRoleById(int id);

    UserRole addRole(UserRole userRole);
}
