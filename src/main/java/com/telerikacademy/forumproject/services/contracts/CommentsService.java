package com.telerikacademy.forumproject.services.contracts;

import com.telerikacademy.forumproject.models.Comments;
import com.telerikacademy.forumproject.models.Posts;
import com.telerikacademy.forumproject.models.Users;

import java.util.List;

public interface CommentsService {
    List<Comments> getAllComments();

    Comments getCommentById(int commentId);

    List<Comments> getCommentsOfPost(int postId);

    void addCommentToPost(Comments comment);

    void setDefaultUserToComments(int userId);

    List<Comments> getCommentsByAuthor(int userId);

    void updatePost(Comments comment, Users operatingUser, Posts postId);

    void deleteComment(int commentId, Users user);
}
