package com.telerikacademy.forumproject.services.contracts;

import com.telerikacademy.forumproject.models.Categories;

import java.util.List;

public interface CategoryService {
    List<Categories> getAllCategories();

    Categories getCategoryById(int categoryId);
}
