package com.telerikacademy.forumproject.services.contracts;

import com.telerikacademy.forumproject.models.Users;

import java.util.List;
import java.util.Optional;

public interface UserService {
    List<Users> getAllUsers();

    Users getUserById(int id);

    void createUser(Users user);

    void updateUser(Users userToUpdate, Users userToAccess);

    void delete(int id, Users userToAccess);

    Users getUserByUsername(String name);

    void blockUser(Users operatingUser, int id);

    void unblockUser(Users operatingUser, int id);

    List<Users> search(Optional<String> search, Users operatingUser);

    boolean isAdmin(Users user);

    void setProfilePicture(int id, String pictureURL, Users operatingUser);

}
