package com.telerikacademy.forumproject.services.contracts;

import com.telerikacademy.forumproject.models.AdminsInfo;
import com.telerikacademy.forumproject.models.Users;

import java.util.List;

public interface AdminsInfoService {
    List<AdminsInfo> getAllAdminsInfo();

    void createAdminsInfo(AdminsInfo adminsInfo);

    void promoteUser(Users operatingUser, int id, AdminsInfo adminsInfo);
}
