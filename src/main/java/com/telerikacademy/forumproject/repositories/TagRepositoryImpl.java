package com.telerikacademy.forumproject.repositories;

import com.telerikacademy.forumproject.exceptions.EntityNotFoundException;
import com.telerikacademy.forumproject.models.Posts;
import com.telerikacademy.forumproject.models.Tags;
import com.telerikacademy.forumproject.repositories.contracts.PostsRepository;
import com.telerikacademy.forumproject.repositories.contracts.TagRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TagRepositoryImpl implements TagRepository {

    private final SessionFactory sessionFactory;
    private final PostsRepository postsRepository;

    @Autowired
    public TagRepositoryImpl(SessionFactory sessionFactory, PostsRepository postsRepository) {
        this.sessionFactory = sessionFactory;
        this.postsRepository = postsRepository;
    }

    @Override
    public List<Tags> getAllTags() {
        try (Session session = sessionFactory.openSession()) {
            Query<Tags> query = session.createQuery("from Tags", Tags.class);
            return query.list();
        }
    }

    @Override
    public Tags getTagById(int tagId) {
        try (Session session = sessionFactory.openSession()) {
            Tags tag = session.get(Tags.class, tagId);

            if (tag == null) {
                throw new EntityNotFoundException("Tag", tagId);
            }

            return tag;
        }
    }

    public Tags create(Tags tag) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(tag);
            session.getTransaction().commit();
        }
        return tag;
    }

    public void update(Tags tag) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(tag);
            session.getTransaction().commit();
        }
    }

    @Override
    public void deleteTagsOfPost(int postId) {
        try (Session session = sessionFactory.openSession()) {
            Posts post = postsRepository.getPostById(postId);
            session.beginTransaction();

            post.getTagsList().clear();

            session.update(post);

            session.getTransaction().commit();
        }
    }

    public Tags getTagByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Tags> query = session.createQuery("from Tags where tagName = :name", Tags.class);
            query.setParameter("name", name);

            if (query.list().isEmpty()) {
                throw new EntityNotFoundException("Tag", "name", name);
            }

            return query.list().get(0);
        }
    }

    @Override
    public void deleteTag(Tags tagToDelete) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();

            session.delete(tagToDelete);

            session.getTransaction().commit();
        }
    }

    public void deleteTagsList(Tags tagToDelete) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();

            for (Posts post : tagToDelete.getPosts()) {
                post.getTagsList().remove(tagToDelete);
                session.update(post);
            }

            session.getTransaction().commit();
        }
    }
}
