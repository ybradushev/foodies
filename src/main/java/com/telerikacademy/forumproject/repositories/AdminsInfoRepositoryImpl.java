package com.telerikacademy.forumproject.repositories;

import com.telerikacademy.forumproject.models.AdminsInfo;
import com.telerikacademy.forumproject.models.Users;
import com.telerikacademy.forumproject.repositories.contracts.AdminsInfoRepository;
import com.telerikacademy.forumproject.repositories.contracts.UserRolesRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AdminsInfoRepositoryImpl implements AdminsInfoRepository {

    private final SessionFactory sessionFactory;
    private final UserRolesRepository userRolesRepository;

    public AdminsInfoRepositoryImpl(SessionFactory sessionFactory, UserRolesRepository userRolesRepository) {
        this.sessionFactory = sessionFactory;
        this.userRolesRepository = userRolesRepository;
    }

    @Override
    public List<AdminsInfo> getAllAdminsInfo() {
        try (Session session = sessionFactory.openSession()) {
            Query<AdminsInfo> query = session.createQuery("from AdminsInfo ", AdminsInfo.class);

            return query.list();
        }
    }

    @Override
    public AdminsInfo createAdmin(AdminsInfo adminsInfo) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(adminsInfo);
            session.getTransaction().commit();
        }

        return adminsInfo;
    }

    @Override
    public void promoteUser(AdminsInfo createdAdminsInfo, Users user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();

            user.setUserRole(userRolesRepository.getRoleById(2));
            user.setAdminsInfo(createdAdminsInfo);
            createdAdminsInfo.setUser(user);

            session.update(createdAdminsInfo);
            session.update(user);
            session.getTransaction().commit();
        }
    }


}
