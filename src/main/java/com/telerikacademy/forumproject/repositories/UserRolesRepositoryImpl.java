package com.telerikacademy.forumproject.repositories;

import com.telerikacademy.forumproject.exceptions.EntityNotFoundException;
import com.telerikacademy.forumproject.models.UserRole;
import com.telerikacademy.forumproject.repositories.contracts.UserRolesRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class UserRolesRepositoryImpl implements UserRolesRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public UserRolesRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public UserRole getRoleById(int id) {
        try (Session session = sessionFactory.openSession()) {
            UserRole userRole = session.get(UserRole.class, id);

            if (userRole == null) {
                throw new EntityNotFoundException("User role", id);
            }

            return userRole;
        }
    }

    @Override
    public UserRole getRoleByName(String roleName) {
        try (Session session = sessionFactory.openSession()) {
            Query<UserRole> query = session.createQuery(
                    "from UserRole where userRoleName = :userRoleName",
                    UserRole.class);
            UserRole userRole = query.list().get(0);

            if (userRole == null) {
                throw new EntityNotFoundException("User role", "name", roleName);
            }

            return userRole;

        }
    }

    @Override
    public UserRole addRole(UserRole userRole) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(userRole);
            session.getTransaction().commit();
        }
        return userRole;
    }
}
