package com.telerikacademy.forumproject.repositories;

import com.telerikacademy.forumproject.exceptions.EntityNotFoundException;
import com.telerikacademy.forumproject.models.Categories;
import com.telerikacademy.forumproject.repositories.contracts.CategoryRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CategoryRepositoryImpl implements CategoryRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public CategoryRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Categories> getAllCategories() {
        try (Session session = sessionFactory.openSession()) {
            Query<Categories> query = session.createQuery("from Categories", Categories.class);
            return query.list();
        }
    }

    @Override
    public Categories getCategoryById(int categoryId) {
        try (Session session = sessionFactory.openSession()) {
            Categories category = session.get(Categories.class, categoryId);

            if (category == null) {
                throw new EntityNotFoundException("Category", categoryId);
            }

            return category;
        }
    }
}
