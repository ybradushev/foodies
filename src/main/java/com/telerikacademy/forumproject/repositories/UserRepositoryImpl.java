package com.telerikacademy.forumproject.repositories;

import com.telerikacademy.forumproject.exceptions.EntityNotFoundException;
import com.telerikacademy.forumproject.models.Users;
import com.telerikacademy.forumproject.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class UserRepositoryImpl implements UserRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Users> getAllUsers() {
        try (Session session = sessionFactory.openSession()) {
            Query<Users> query = session.createQuery("from Users", Users.class);
            return query.list();
        }
    }

    @Override
    public Users getUserById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Users user = session.get(Users.class, id);
            if (user == null) {
                throw new EntityNotFoundException("User", id);
            }
            return user;
        }
    }

    @Override
    public Users createUser(Users user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(user);
            session.getTransaction().commit();
        }
        return user;
    }

    @Override
    public void updateUser(Users user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public void deleteUser(int id) {
        Users userToDelete = getUserById(id);

        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(userToDelete);
            session.getTransaction().commit();
        }
    }

    @Override
    public Users getUserByUsername(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Users> query = session.createQuery(
                    "from Users where username = :username", Users.class);
            query.setParameter("username", name);


            if (query.list().isEmpty()) {
                throw new EntityNotFoundException("User", "username", name);
            }

            return query.list().get(0);
        }
    }

    @Override
    public Users getUserByEmail(String email) {
        try (Session session = sessionFactory.openSession()) {
            Query<Users> query = session.createQuery(
                    "from Users where email = :email", Users.class);
            query.setParameter("email", email);


            if (query.list().isEmpty()) {
                throw new EntityNotFoundException("User", "email", email);
            }

            return query.list().get(0);
        }
    }

    //TODO
    public Users checkCredentials(String username, String password) {
        try (Session session = sessionFactory.openSession()) {
            Query<Users> query = session.createQuery(
                    "from Users u where u.username = :username and u.password = :password", Users.class);
            query.setParameter("username", username);
            query.setParameter("password", password);

            if (query.list().get(0) == null) {
                throw new EntityNotFoundException("User", "credentials", username + " and " + password);
            }

            return query.list().get(0);
        }
    }

    @Override
    public void blockUser(int id) {
        try (Session session = sessionFactory.openSession()) {
            Users userToBlock = getUserById(id);
            userToBlock.setUserBlocked(true);

            session.beginTransaction();
            session.update(userToBlock);
            session.getTransaction().commit();
        }
    }

    @Override
    public void unblockUser(int id) {
        try (Session session = sessionFactory.openSession()) {
            Users userToBlock = getUserById(id);
            userToBlock.setUserBlocked(false);

            session.beginTransaction();
            session.update(userToBlock);
            session.getTransaction().commit();
        }
    }

    public List<Users> search(Optional<String> search) {
        if (search.isEmpty()) {
            return getAllUsers();
        }

        try (Session session = sessionFactory.openSession()) {
            Query<Users> query = session.createQuery("from Users where username like :name or " +
                    "email like :name or " +
                    "firstName like :name", Users.class);

            query.setParameter("name", "%" + search.get() + "%");

            return query.list();
        }
    }

    @Override
    public void setProfilePicture(int userId, String pictureURL) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();

            Users user = getUserById(userId);
            user.setProfilePictureUrl(pictureURL);

            session.update(user);
            session.getTransaction().commit();
        }
    }
}
