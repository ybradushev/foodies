package com.telerikacademy.forumproject.repositories.contracts;

import com.telerikacademy.forumproject.models.AdminsInfo;
import com.telerikacademy.forumproject.models.Users;

import java.util.List;

public interface AdminsInfoRepository {
    List<AdminsInfo> getAllAdminsInfo();

    AdminsInfo createAdmin(AdminsInfo adminsInfo);

    void promoteUser(AdminsInfo createdAdminsInfo, Users user);
}
