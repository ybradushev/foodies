package com.telerikacademy.forumproject.repositories.contracts;

import com.telerikacademy.forumproject.models.Categories;
import com.telerikacademy.forumproject.models.Comments;
import com.telerikacademy.forumproject.models.Posts;
import com.telerikacademy.forumproject.models.Users;

import java.util.List;
import java.util.Optional;

public interface PostsRepository {
    List<Posts> getAllPosts();

    Posts getPostById(int id);

    Posts createPost(Posts post);

    void updatePost(Posts post);

    void deletePost(int id);

    List<Posts> getPostByAuthor(int userId);

    void makePostWithDefaultUser(List<Posts> posts);

    List<Posts> getPostsByLikesList(int userId);

    void deleteLikesOfUser(List<Posts> posts, int userId);

    void deleteLikesOfPost(int postId);

    void replyPost(Comments comment, Users author, int id);

    void likePost(Users operatingUser, Posts postToLike);

    List<Posts> searchPostByTag(Optional<List<String>> tags);

    List<Posts> filter(Optional<String> title, Optional<Integer> authorId, Optional<Integer> categoryId, Optional<String> startDate, Optional<String> endDate, Optional<String> sort);

    List<Posts> updatedFilter(Optional<String> title, Optional<String> authorUsername, Optional<Integer> categoryId, Optional<String> startDate, Optional<String> endDate, Optional<String> sort);

    List<Posts> getTopMostCommentedPosts();

    List<Posts> getTopLikedPosts();

    List<Posts> getRecentlyCreatedPosts();

    Integer getCountOfUsers();

    Integer getCountOfPosts();

    List<Posts> getPostsByCategory(Categories category);

    void unlikePost(Users operatingUser, Posts postToLike);

}
