package com.telerikacademy.forumproject.repositories.contracts;

import com.telerikacademy.forumproject.models.UserRole;

public interface UserRolesRepository {

    UserRole addRole(UserRole userRole);

    UserRole getRoleById(int id);

    UserRole getRoleByName(String roleName);
}
