package com.telerikacademy.forumproject.repositories.contracts;

import com.telerikacademy.forumproject.models.Tags;

import java.util.List;

public interface TagRepository {
    List<Tags> getAllTags();

    Tags getTagById(int tagId);

    void deleteTagsOfPost(int postId);

    Tags create(Tags tag);

    void update(Tags tag);

    Tags getTagByName(String name);

    void deleteTag(Tags tagToDelete);

    void deleteTagsList(Tags tagToDelete);
}
