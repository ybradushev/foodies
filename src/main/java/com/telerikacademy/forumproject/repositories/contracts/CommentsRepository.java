package com.telerikacademy.forumproject.repositories.contracts;

import com.telerikacademy.forumproject.models.Comments;

import java.util.List;

public interface CommentsRepository {
    Comments getCommentById(int commentId);

    List<Comments> getCommentOfPost(int postId);

    Comments addCommentToPost(Comments comment);

    List<Comments> getCommentsByAuthor(int userId);

    void setDefaultUserToComments(List<Comments> comments);

    void deleteCommentsOfPost(int postId);

    void updateComment(Comments comment);

    List<Comments> getAllComments();

    void deleteComment(int commentId);
}
