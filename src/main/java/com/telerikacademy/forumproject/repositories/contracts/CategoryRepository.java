package com.telerikacademy.forumproject.repositories.contracts;

import com.telerikacademy.forumproject.models.Categories;

import java.util.List;

public interface CategoryRepository {
    List<Categories> getAllCategories();

    Categories getCategoryById(int categoryId);
}
