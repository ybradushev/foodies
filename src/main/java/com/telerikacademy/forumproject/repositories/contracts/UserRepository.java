package com.telerikacademy.forumproject.repositories.contracts;

import com.telerikacademy.forumproject.models.Users;

import java.util.List;
import java.util.Optional;

public interface UserRepository {
    List<Users> getAllUsers();

    Users getUserById(int id);

    Users createUser(Users user);

    void updateUser(Users user);

    void deleteUser(int id);

    Users getUserByUsername(String name);

    Users getUserByEmail(String email);

    void blockUser(int id);

    void unblockUser(int id);

    List<Users> search(Optional<String> search);

    void setProfilePicture(int userId, String pictureURL);
}
