package com.telerikacademy.forumproject.repositories;

import com.telerikacademy.forumproject.exceptions.EntityNotFoundException;
import com.telerikacademy.forumproject.models.Categories;
import com.telerikacademy.forumproject.models.Comments;
import com.telerikacademy.forumproject.models.Posts;
import com.telerikacademy.forumproject.models.Users;
import com.telerikacademy.forumproject.repositories.contracts.PostsRepository;
import com.telerikacademy.forumproject.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;


@Repository
public class PostsRepositoryImpl implements PostsRepository {

    private final UserRepository userRepository;
    private final SessionFactory sessionFactory;

    @Autowired
    public PostsRepositoryImpl(UserRepository repository, SessionFactory sessionFactory) {
        this.userRepository = repository;
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Posts> getAllPosts() {
        try (Session session = sessionFactory.openSession()) {
            Query<Posts> query = session.createQuery("from Posts", Posts.class);
            return query.list();
        }
    }

    public List<Posts> getTopMostCommentedPosts() {
        try (Session session = sessionFactory.openSession()) {
            Query<Posts> query = session.createQuery("from Posts p order by size(p.comments) desc", Posts.class);
            query.setMaxResults(10);

            return query.list();
        }
    }


    public List<Posts> getTopLikedPosts() {
        try (Session session = sessionFactory.openSession()) {
            Query<Posts> query = session.createQuery("from Posts p order by size(p.likesList) desc", Posts.class);
            query.setMaxResults(10);

            return query.list();
        }
    }

    public List<Posts> getRecentlyCreatedPosts() {
        try (Session session = sessionFactory.openSession()) {
            Query<Posts> query = session.createQuery("from Posts order by postTimestamp desc", Posts.class);
            query.setMaxResults(10);

            return query.list();
        }
    }

    public Integer getCountOfUsers() {
        try (Session session = sessionFactory.openSession()) {
            Query<Integer> query = session.createQuery("select count(*) from Users", Integer.class);

            return query.getSingleResult();
        }
    }

    public Integer getCountOfPosts() {
        try (Session session = sessionFactory.openSession()) {
            Query<Integer> query = session.createQuery("select count(*) from Posts", Integer.class);

            return query.getSingleResult();
        }
    }

    @Override
    public List<Posts> getPostsByCategory(Categories category) {
        try (Session session = sessionFactory.openSession()) {
            Query<Posts> query = session.createQuery("from Posts where category.categoryId =: categoryId", Posts.class);
            query.setParameter("categoryId", category.getCategoryId());

            return query.list();
        }
    }

    @Override
    public List<Posts> searchPostByTag(Optional<List<String>> tags) {
        if (tags.isEmpty()) {
            return getAllPosts();
        }

        try (Session session = sessionFactory.openSession()) {
            Query<Posts> query = session.createQuery(
                    "select distinct p from Posts p inner join p.tagsList t where t.tagName in (:tags)"
                    , Posts.class);
            query.setParameterList("tags", tags.get());
            return query.list();
        }
    }

    @Override
    public List<Posts> filter(Optional<String> title, Optional<Integer> authorId, Optional<Integer> categoryId, Optional<String> startDate, Optional<String> endDate, Optional<String> sort) {
        try (Session session = sessionFactory.openSession()) {
            var queryString = new StringBuilder(" from Posts ");
            var filter = new ArrayList<String>();
            var queryParams = new HashMap<String, Object>();

            title.ifPresent(value -> {
                filter.add(" postTitle like :title ");
                queryParams.put("title", "%" + value + "%");
            });

            categoryId.ifPresent(value -> {
                filter.add(" category.categoryId = :categoryId");
                queryParams.put("categoryId", value);
            });

            authorId.ifPresent(value -> {
                filter.add(" postAuthor.userId = :authorId");
                queryParams.put("authorId", value);
            });

            if (startDate.isPresent() && endDate.isPresent()) {
                filter.add(generateDateQueryString(startDate, endDate));
            }


            if (!filter.isEmpty()) {
                queryString.append("where ").append(String.join(" and ", filter));
                System.out.println(queryString);
            }

            sort.ifPresent(value -> {
                queryString.append(generateSortString(value));
            });

            Query<Posts> queryList = session.createQuery(queryString.toString(), Posts.class);
            queryList.setProperties(queryParams);

            System.out.println(queryString);
            return queryList.list();
        }
    }

    @Override
    public List<Posts> updatedFilter(Optional<String> title, Optional<String> authorUsername, Optional<Integer> categoryId, Optional<String> startDate, Optional<String> endDate, Optional<String> sort) {
        try (Session session = sessionFactory.openSession()) {
            var queryString = new StringBuilder(" from Posts ");
            var filter = new ArrayList<String>();
            var queryParams = new HashMap<String, Object>();

            title.ifPresent(value -> {
                if (!title.get().equals("")) {
                    filter.add(" postTitle like :title ");
                    queryParams.put("title", "%" + value + "%");
                }
            });

            authorUsername.ifPresent(value -> {
                if (!authorUsername.get().equals("")) {
                    filter.add(" postAuthor.username = :authorUsername");
                    queryParams.put("authorUsername", value);
                }
            });

            categoryId.ifPresent(value -> {
                filter.add(" category.categoryId = :categoryId");
                queryParams.put("categoryId", value);
            });

            if (startDate.isPresent() || endDate.isPresent()) {
                if (!startDate.get().equals("") && !endDate.get().equals("")) {
                    filter.add(generateDateQueryString(startDate, endDate));
                }
            }


            if (!filter.isEmpty()) {
                queryString.append("where ").append(String.join(" and ", filter));
                System.out.println(queryString);
            }

            sort.ifPresent(value -> {
                queryString.append(generateSortString(value));
            });

            System.out.println(queryString);

            Query<Posts> queryList = session.createQuery(queryString.toString(), Posts.class);
            queryList.setProperties(queryParams);


            return queryList.list();
        }
    }

    @Override
    public Posts getPostById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Posts post = session.get(Posts.class, id);
            if (post == null) {
                throw new EntityNotFoundException("Post", id);
            }
            return post;
        }
    }

    @Override
    public Posts createPost(Posts post) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(post);
            session.getTransaction().commit();
        }
        return post;
    }

    @Override
    public void updatePost(Posts post) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(post);
            session.getTransaction().commit();
        }
    }

    @Override
    public void deletePost(int id) {
        Posts postToDelete = getPostById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(postToDelete);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<Posts> getPostByAuthor(int userId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Posts> query = session.createQuery("from Posts where postAuthor.id = :userId", Posts.class);
            query.setParameter("userId", userId);

            return query.list();
        }
    }

    @Override
    public void makePostWithDefaultUser(List<Posts> posts) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            for (Posts post : posts) {
                session.update(post);
            }
            session.getTransaction().commit();
        }
    }

    @Override
    public List<Posts> getPostsByLikesList(int userId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Posts> query = session.createQuery(
                    "select p from Posts p join p.likesList l where l.id = :userId", Posts.class);
            query.setParameter("userId", userId);
            return query.getResultList();
        }
    }

    @Override
    public void deleteLikesOfUser(List<Posts> posts, int userId) {
        Users user = userRepository.getUserById(userId);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            for (Posts post : posts) {
                post.getLikesList().remove(user);
                session.update(post);
            }
            session.getTransaction().commit();
        }
    }

    @Override
    public void deleteLikesOfPost(int postId) {
        Posts post = getPostById(postId);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            post.getLikesList().clear();
            session.update(post);
            session.getTransaction().commit();
        }
    }

    @Override
    public void replyPost(Comments comment, Users author, int id) {
        Posts post = getPostById(id);

        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            comment.setCommentAuthor(author);
            comment.setPost(post);
            post.getComments().add(comment);

            session.save(comment);
            session.update(post);

            session.getTransaction().commit();
        }
    }

    @Override
    public void likePost(Users operatingUser, Posts postToLike) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();

            operatingUser.getLikes().add(postToLike);
            postToLike.getLikesList().add(operatingUser);

            session.update(operatingUser);
            session.update(postToLike);
            session.getTransaction().commit();
        }
    }

    @Override
    public void unlikePost(Users operatingUser, Posts postToUnlike) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();

            operatingUser.getLikes().remove(postToUnlike);
            postToUnlike.getLikesList().remove(operatingUser);

            session.update(operatingUser);
            session.update(postToUnlike);
            session.getTransaction().commit();
        }
    }

    public String generateDateQueryString(Optional<String> startDate, Optional<String> endDate) {
        var queryString = new StringBuilder();

        if (startDate.isEmpty() && endDate.isEmpty()) {
            return queryString.toString();
        }

        if (startDate.isPresent() && endDate.isPresent()) {
            if (!startDate.get().equals("") && !endDate.get().equals("")) {
                queryString.append(" postTimestamp between ")
                        .append("'" + startDate.get() + "'" + " and ")
                        .append("'" + endDate.get() + "'");
                return queryString.toString();
            }
        }

        if (startDate.isPresent() && endDate.isEmpty()) {
            if (!startDate.get().equals("")) {
                queryString.append("postTimestamp >= ")
                        .append("'" + startDate.get() + "'");
            }
        }

        if (startDate.isEmpty() && endDate.isPresent()) {
            if (!endDate.get().equals("")) {
                queryString.append(" postTimestamp <= ")
                        .append("'" + endDate.get() + "'");
            }
        }

        return queryString.toString();
    }

    private String generateSortString(String value) {
        var queryString = new StringBuilder(" order by ");
        String[] params = value.split("_");

        if (value.isEmpty()) {
            // return " order by name ";
            throw new UnsupportedOperationException("Sort should have maximum two params divided by _ symbol");
        }

        switch (params[0]) {
            case "title":
                queryString.append(" postTitle ");
                break;
            case "author":
                queryString.append(" postAuthor.username ");
                break;
            case "category":
                queryString.append(" category.categoryName ");
                break;
            case "date":
                queryString.append(" postTimestamp ");
                break;
        }

        if (params.length > 1 && params[1].equals("desc")) {
            queryString.append(" desc ");
        }
        if (params.length > 2) {
            throw new UnsupportedOperationException("Sort should have maximum two params divided by _ symbol");
        }

        return queryString.toString();
    }

}
