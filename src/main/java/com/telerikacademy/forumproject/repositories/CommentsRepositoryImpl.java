package com.telerikacademy.forumproject.repositories;

import com.telerikacademy.forumproject.exceptions.EntityNotFoundException;
import com.telerikacademy.forumproject.models.Comments;
import com.telerikacademy.forumproject.models.Posts;
import com.telerikacademy.forumproject.models.Users;
import com.telerikacademy.forumproject.repositories.contracts.CommentsRepository;
import com.telerikacademy.forumproject.repositories.contracts.PostsRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CommentsRepositoryImpl implements CommentsRepository {
    private final SessionFactory sessionFactory;
    private final PostsRepository postsRepository;

    @Autowired
    public CommentsRepositoryImpl(SessionFactory sessionFactory, PostsRepository postsRepository) {
        this.sessionFactory = sessionFactory;
        this.postsRepository = postsRepository;
    }

    @Override
    public List<Comments> getAllComments() {
        try (Session session = sessionFactory.openSession()) {
            Query<Comments> query = session.createQuery("from Comments", Comments.class);
            return query.list();
        }
    }

    public Comments getCommentById(int commentId) {
        try (Session session = sessionFactory.openSession()) {
            Comments comment = session.get(Comments.class, commentId);

            if (comment == null) {
                throw new EntityNotFoundException("Comment", commentId);
            }

            return comment;
        }
    }

    @Override
    public List<Comments> getCommentOfPost(int postId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Comments> query = session.createQuery("from Comments where post.postId = :postId", Comments.class);
            return query.list();
        }
    }

    @Override
    public Comments addCommentToPost(Comments comment) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(comment);
            session.getTransaction().commit();
        }
        return comment;
    }

    @Override
    public void setDefaultUserToComments(List<Comments> comments) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            for (Comments comment : comments) {
                session.update(comment);
            }

            session.getTransaction().commit();
        }
    }

    @Override
    public List<Comments> getCommentsByAuthor(int userId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Comments> query = session.createQuery("from Comments where commentAuthor.id = :userId", Comments.class);
            query.setParameter("userId", userId);
            return query.list();
        }
    }

    @Override
    public void deleteCommentsOfPost(int postId) {
        try (Session session = sessionFactory.openSession()) {
            Posts post = postsRepository.getPostById(postId);
            session.beginTransaction();

            for (Comments comment : post.getComments()) {
                session.delete(comment);
            }

            session.update(post);

            session.getTransaction().commit();
        }
    }

    @Override
    public void updateComment(Comments comment) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(comment);
            session.getTransaction().commit();
        }
    }

    @Override
    public void deleteComment(int commentId){
        Comments commentToDelete = getCommentById(commentId);

        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(commentToDelete);
            session.getTransaction().commit();
        }
    }

}
